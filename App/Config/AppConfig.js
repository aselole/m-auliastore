// Simple React Native specific changes

export default {
  // font scaling override - RN default is on
  allowTextFontScaling: true,
  baseServerURL: "http://aulia2.idcosci.com/index.php/",
  homeCarousel: "ecp_home/carousel/get_many?get-carousel",
}
