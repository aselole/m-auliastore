import React, { Component } from 'react'
import { View, ScrollView, TouchableOpacity, TextInput } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Metrics, Servers, Strings } from 'Theme'
import {
  ImageFitLoading, ImageLoading, Text, TextJustify,
  ListFilter, Loading, InfoScreen, ToolbarButton, ButtonIcon
} from 'Template'
import { ImageList } from 'Component'
import AppConfig from 'Config'
import { isPropsChanged } from 'Lib/CheckUtils'
import { convertToRupiah } from 'Lib/NumberUtils'
import { getReadableDate, getIntervalTimeToday } from 'Lib/DateUtils'
import Parallax from 'Lib/react-native-parallax'
import ListAction from 'Redux/ListRedux'
import TransactionAction from 'Redux/TransactionRedux'
import UserAction from 'Redux/UserRedux'
import StarRating from 'react-native-star-rating'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class ArticleDetail extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Artikel dan Berita',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor],
      headerLeft:
        <ToolbarButton
          onPress={() => navigation.goBack()}
          image={Images.left}
          styleContainer={stylesCommon.toolbarLeftContainer}
          styleContentContainer={{flex:0.5}}
        />
    }
  }

  constructor(props) {
    super(props)
    this.state = {
      comment: null,
      input_comment: null,
      enable_comment: false,
      is_send_disabled: true,
    }
  }

  componentDidMount() {
    this.props.isSignedIn()
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      if(nextProps.data.hasOwnProperty('comment'))
        this.setState({ comment: nextProps.data.comment })
    }
    if(isPropsChanged(this.props.is_signed_in, nextProps.is_signed_in)) {
      if(nextProps.is_signed_in) this.setState({ enable_comment: true })
    }
  }

  _onTypingInput(message) {
    this.setState({
      input_comment: message,
      is_send_disabled: _.isEmpty(message) ? true : false,
    })
  }

  _renderComment = ({ item, index }) => {
    let imgMetrics = 55
    let borderRadius = 27.5
    return (
      <TouchableOpacity
        style={{flex:1, height:70, marginTop:5, flexDirection:'row', backgroundColor:'white'}}>
        <ImageLoading
          path={item.user_photo_profile}
          styleContainer={{height:imgMetrics, width:imgMetrics, borderRadius:borderRadius, borderWidth:1, borderColor:'gray', marginLeft:10, alignSelf:'center', overflow:'hidden'}}
        />
        <View style={{flex:1, marginLeft:10, justifyContent:'center'}}>
          <View style={{flex:0.7, justifyContent:'center'}}>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              <Text style={{fontWeight:'bold'}}>{item.nama_customer}</Text>
              <Text style={{marginLeft:5, fontSize:12, color:'gray'}}>{getIntervalTimeToday(item.created_at)}</Text>
            </View>
            <Text>{item.content}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }

  _sendComment() {
    this.props.postCommentBerita(
      this.state.input_comment,
      this.props.artikel.id_newsfeed,
      0
    )
  }

  render () {
    return (
      <Parallax.ScrollView style={styles.container}>
        <Parallax.Image
         key={this.props.artikel.id_newsfeed}
         style={styles.parallaxImage}
         overlayStyle={{ backgroundColor: 'rgba(0,0,0,0.3)'}}
         onPress={() => this._onParallaxClicked(item)}
         source={{ uri: this.props.artikel.gambar_berita }}
         parallaxFactor={0.8}>
        </Parallax.Image>
        <View style={stylesCommon.detailTitleContainer}>
          <Text
            numberOfLines={2}
            style={stylesCommon.detailTitle}>
            {this.props.artikel.judul}
          </Text>
        </View>
        <View style={stylesCommon.detailContentContainer}>
          <TextJustify
            text={this.props.artikel.content}
            styleContainer={stylesCommon.detailBody}
          />
          <Text style={stylesCommon.detailContentDate}>
            Ditulis pada tanggal {getReadableDate(this.props.artikel.date_edit, 'id', 'DD MMM YYYY')}
          </Text>
        </View>
        <View style={{flex:1}}>
          <Text style={{marginLeft:12}}>{this.props.artikel.jumlah_comment} Komentar</Text>
          <ListFilter
            onLoadData={() => this.props.getCommentBerita(this.props.artikel.id_newsfeed, 0)}
            data={this.state.comment}
            renderRow={this._renderComment}
            isFetching={this.props.is_fetching}
            keys='id_comment'
          />
          { this.state.enable_comment &&
            <View style={styles.chatInputContainer}>
              <TextInput
                onChangeText={(input_comment) => this._onTypingInput(input_comment)}
                value={this.state.input_comment}
                placeholder='Tulis komentar disini...'
                style={{flex:4, paddingLeft:8, fontSize:14}}
                underlineColorAndroid='rgba(0,0,0,0)' />
              <ButtonIcon
                title='Kirim'
                isLoading={this.props.is_fetching_comment}
                isDisabled={this.state.is_send_disabled}
                onPress={() => this._sendComment()}
                styleContainer={stylesCommon.buttonBottomContainer}
                style={stylesCommon.buttonBottom}
                styleTitle={{fontSize:16}}
                titleColor={Colors.theme}
                bgColor={Colors.themeAction} />
            </View>
          }
        </View>
      </Parallax.ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.list.id,
    is_fetching: state.list.is_fetching,
    artikel: state.list.data_selected,
    data: state.list.data,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
    is_signed_in: state.user.ok_signin,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailProduk: (kode) => dispatch(ListAction.getDetailProduk(kode)),
    getCommentBerita: (id_newsfeed, id_parent) => dispatch(ListAction.getCommentBerita(id_newsfeed, id_parent)),
    postCommentBerita: (content, id_parent, id_newsfeed) => dispatch(TransactionAction.postCommentBerita(content, id_parent, id_newsfeed)),
    isSignedIn: () => dispatch(UserAction.isSignedIn()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleDetail)
