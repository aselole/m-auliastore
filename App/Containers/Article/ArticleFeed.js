import React, { Component } from 'react'
import { View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from 'Theme'
import {
  ImageSwipe, ImageCarousel, ToolbarButton, Text,
  Loading, InfoScreen, ListFilter, ImageLoading,
  ImageFitLoading, TextJustify
} from 'Template'
import { ImageList } from 'Component'
import AppConfig from 'Config'
import { isPropsChanged } from 'Lib/CheckUtils'
import Parallax from 'Lib/react-native-parallax'
import UserAction from 'Redux/UserRedux'
import ListAction from 'Redux/ListRedux'
import { getReadableDate } from 'Lib/DateUtils'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class ArticleFeed extends Component {

  // it actually style it's own navigator, i.e. HomeStack on TabNavigation.js
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Berita dan Artikel',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor]
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      data: null
    }
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      if(nextProps.data.hasOwnProperty('artikel')) {
        this.setState({ data: nextProps.data['artikel'] })
      }
    }
  }

  _batchData(start, length) {
    this.props.getListBerita(start, length)
  }

  _onListClicked(item) {
    this.props.onListItemClicked(item)
    this.props.navigation.navigate('ArticleDetail')
  }

  _renderList = ({ item, index }) => {
    return (
        <TouchableOpacity
          style={styles.listContainer}
          onPress={() => this._onListClicked(item)}>
          <View style={styles.listImage}>
            <ImageFitLoading
              urlDefault={Images.no_photo}
              path={item.gambar_berita} />
          </View>

          <View style={styles.listContent}>
            <View style={styles.listBody}>
              <Text style={styles.listTitle} numberOfLines={2}>
                {item.judul}
              </Text>
              <Text style={styles.listDate} numberOfLines={1}>
                {getReadableDate(item.date_edit)}
              </Text>
            </View>
            { item.jumlah_comment !== '0' &&
              <View style={styles.listAction}>
                <Image source={Images.comment} style={{width:25, height:25, marginRight:5}} />
                <Text>{item.jumlah_comment}</Text>
              </View>
            }
          </View>
        </TouchableOpacity>
    )
  }

  render () {
    return (
     <ScrollView
       style={styles.container}
       contentContainerStyle={styles.containerContent}>
        <ListFilter
          data={this.state.data}
          errorInfo={this.props.error_info}
          errorTitle={this.props.error_title}
          isFetching={this.props.is_fetching}
          renderRow={this._renderList.bind(this)}
          keys='id_newsfeed'
          loadingType='wave'
          isLoadBatch={true}
          onLoadData={(start, length) => this._batchData(start, length)}
        />
     </ScrollView>
   )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.list.data,
    is_fetching: state.list.is_fetching,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getListBerita: (start, length) => dispatch(ListAction.getListBerita(start, length)),
    onListItemClicked: (data_selected) => dispatch(ListAction.onListItemClicked(data_selected)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleFeed)
