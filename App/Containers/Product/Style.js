import { StyleSheet } from 'react-native'
import { Colors, Metrics } from 'Theme'
import React, { Dimensions, PixelRatio } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 5,
  },
  containerContent: {

  },
  ratingContainer: {
    backgroundColor: 'yellow',
    width: 200
  }
})
