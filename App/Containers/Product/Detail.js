import React, { Component } from 'react'
import { View, ScrollView } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Metrics, Servers, Strings } from 'Theme'
import {
  ImageCarousel, ToolbarButton, Text, Loading, InfoScreen
} from 'Template'
import { ImageList, ItemCounter } from 'Component'
import AppConfig from 'Config'
import { isPropsChanged } from 'Lib/CheckUtils'
import { convertToRupiah } from 'Lib/NumberUtils'
import Parallax from 'Lib/react-native-parallax'
import ListAction from 'Redux/ListRedux'
import StarRating from 'react-native-star-rating'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class Detail extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Aulia Store',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor],
      headerLeft:
        <ToolbarButton
          onPress={() => navigation.goBack()}
          image={Images.left}
          styleContainer={stylesCommon.toolbarLeftContainer}
          styleContentContainer={{flex:0.5}}
        />
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      count: 0
    }
  }

  componentDidMount() {
    this.props.getDetailProduk(this.props.id)
  }

  // ketika jumlah produk yg dibeli berubah
  _onCountChanged(number) {
    this.setState({ count: number })
  }

  render () {
    if(this.props.data) {
      return (
       <ScrollView
         style={styles.container}
         contentContainerStyle={styles.containerContent}>
         <View style={{flex:1}}>
           <ImageCarousel
             data={this.props.data.gambar}
             defaultImage={Images.no_photo}
             itemKey='path'
             styleCarousel={{paddingTop:15}}
             borderRadius={15}
           />
         </View>
         <Text>{convertToRupiah(this.props.data.price)}</Text>
         <Text>{this.props.data.nama_product}</Text>
         <StarRating
            disabled={true}
            maxStars={5}
            rating={parseInt(this.props.data.rating_value, 10)}
            containerStyle={{width: Metrics.vw * 20}}
            starSize={25}
          />
         <ItemCounter
           onValueChanged={(number) => this._onCountChanged(number)}
           isVertical={false}
         />
         <Text>{this.props.data.short_information}</Text>
         <Text>{this.props.data.description}</Text>
         <Text>{this.props.data.additional_information}</Text>
         <Text>{this.props.data.review_counter} review</Text>

       </ScrollView>
      )
    } else if(this.props.error_title) {
      return (
        <View style={styles.container}>
          <InfoScreen
            icon={Images.no_photo}
            title={this.props.error_title}
            body={this.props.error_info} />
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
          <Loading />
        </View>
      )
    }
  }
}

const mapStateToProps = (state) => {
  return {
    id: state.list.id,
    is_fetching: state.list.is_fetching,
    data: state.list.detail,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDetailProduk: (kode) => dispatch(ListAction.getDetailProduk(kode)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)
