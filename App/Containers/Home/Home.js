import React, { Component } from 'react'
import { View, ScrollView, Image } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from 'Theme'
import { ImageSwipe, ImageCarousel, ToolbarButton, Text } from 'Template'
import { ImageList } from 'Component'
import AppConfig from 'Config'
import { isPropsChanged } from 'Lib/CheckUtils'
import Parallax from 'Lib/react-native-parallax'
import UserAction from 'Redux/UserRedux'
import ListAction from 'Redux/ListRedux'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class Home extends Component {

  // it actually style it's own navigator, i.e. HomeStack on TabNavigation.js
  static navigationOptions = ({navigation}) => {
    return {
      title: 'Aulia Store',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor]
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      data_carousel: null,
      data_best_seller: null,
      data_new_arrival: null,
      data_most_review: null,
    }
  }

  componentDidMount() {
    this.props.getCarousel()
    this.props.getBestSeller()
    this.props.getNewArrival()
    this.props.getMostReview()
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      if(nextProps.data.hasOwnProperty('carousel')) {
        this.setState({ data_carousel: nextProps.data['carousel'] })
      } if(nextProps.data.hasOwnProperty('best_seller')) {
        this.setState({ data_best_seller: nextProps.data['best_seller'] })
      } if(nextProps.data.hasOwnProperty('new_arrival')) {
        this.setState({ data_new_arrival: nextProps.data['new_arrival'] })
      } if(nextProps.data.hasOwnProperty('most_review')) {
        this.setState({ data_most_review: nextProps.data['most_review'] })
      }
    }
  }

  _onPressProduct(item, index) {
    this.props.onListClicked(item.kode_product)
    this.props.navigation.navigate('Detail')
  }

  _onPressCarousel(item, index) {
    if(_.isNil(item.kode_nama_product)) return
    let indexRemoved = item.kode_nama_product.indexOf(' - ')
    let kode = item.kode_nama_product.substring(0, indexRemoved)
    this.props.onListClicked(kode)
    this.props.navigation.navigate('Detail')
  }

  render () {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.containerContent}>

        <ImageCarousel
          data={this.state.data_carousel}
          defaultImage={Images.no_photo}
          itemKey='path_image'
          styleCarousel={{paddingTop:15}}
          borderRadius={15}
          onPress={(item, index) => this._onPressCarousel(item, index)}
        />

        <Text style={styles.slideTitle}>BEST SELLER</Text>
        <ImageList
          data={this.state.data_best_seller}
          defaultImage={Images.no_photo}
          itemKey='thumbnail_path'
          styleImageContainer={styles.slideImageContainer}
          styleImageTitle={styles.imageName}
          styleImagePrice={styles.imagePrice}
          onPress={(item, index) => this._onPressProduct(item, index)}
        />

        <Text style={styles.slideTitle}>NEW ARRIVAL</Text>
        <ImageList
          data={this.state.data_new_arrival}
          defaultImage={Images.no_photo}
          itemKey='thumbnail_path'
          styleImageContainer={styles.slideImageContainer}
          styleImageTitle={styles.imageName}
          styleImagePrice={styles.imagePrice}
          onPress={(item, index) => this._onPressProduct(item, index)}
        />

        <Text style={styles.slideTitle}>MOST REVIEW</Text>
        <ImageList
          data={this.state.data_most_review}
          defaultImage={Images.no_photo}
          itemKey='thumbnail_path'
          styleImageContainer={styles.slideImageContainer}
          styleImageTitle={styles.imageName}
          styleImagePrice={styles.imagePrice}
          onPress={(item, index) => this._onPressProduct(item, index)}
        />

      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.list.data,
    error_info: state.list.error_info,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCarousel: () => dispatch(ListAction.getCarousel()),
    getBestSeller: () => dispatch(ListAction.getBestSellerHome()),
    getNewArrival: () => dispatch(ListAction.getNewArrivalHome()),
    getMostReview: () => dispatch(ListAction.getMostReviewHome()),
    onListClicked: (id) => dispatch(ListAction.onListClicked(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
