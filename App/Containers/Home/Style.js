import { StyleSheet } from 'react-native'
import { Colors, Metrics } from 'Theme'
import React, { Dimensions, PixelRatio } from 'react-native'

export default StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingTop: 5,
  },
  containerContent: {

  },
  parallaxContainer: {
    width: Metrics.deviceWidth,
    height: Metrics.deviceWidth,
  },
  slideImageContainer: {
    width: Metrics.vw * 40,
    margin: 5,
  },
  slideImageContent: {

  },
  slideTitle: {
    marginVertical: 15,
    alignSelf: 'center',
  },
  imageName: {
    "textAlign": "center",
    "fontSize": 13,
    "width": Metrics.vw * 40,
    "paddingLeft": 6,
    "paddingTop": 6
  },
  imagePrice: {
      "color": "#999",
      "fontSize": 12,
      "textAlign": "center",
      "paddingTop": 6
  },
})
