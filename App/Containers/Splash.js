import React, { Component } from 'react'
import { Picker, ScrollView, Text, Image, View, TouchableHighlight, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { Button } from 'react-native-elements'
import { Colors, Images, Servers, Strings } from 'Theme'
import UserAction from 'Redux/UserRedux'
import { debugApi } from 'Lib/NetworkUtils'
import { TextInputIcon } from 'Template'
import FlexImage from 'react-native-flex-image'
var _ = require('lodash')

class Tentang extends Component {
  static navigationOptions = {
    header: null,
    headerVisible: false,
  };

  constructor (props) {
    super(props)
  }

  componentDidMount() {
    this.props.navigation.navigate('TabNav')
  }

  render () {
    return (
      <View style={{ flex:1 }}>
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    signed_in: state.user.ok_signin,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tentang)
