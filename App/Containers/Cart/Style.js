import { StyleSheet } from 'react-native'
import { Colors, Metrics } from 'Theme'
import React, { Dimensions, PixelRatio } from 'react-native'

export default StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'#fff'
  },
  scrollContainer: {
    backgroundColor: '#fff',
    paddingTop: 5,
  },
  scrollContainerContent: {
    flex:1,
  },
  parallaxContainer: {
    width: Metrics.deviceWidth,
    height: Metrics.deviceWidth,
  },
  parallaxImage: {
    width: Metrics.deviceWidth,
    height: Metrics.vh * 50,
    borderBottomWidth: 1,
    borderBottomColor: "#fff"
  },
  slideImageContainer: {
    width: Metrics.vw * 40,
    margin: 5,
  },
  slideImageContent: {

  },
  slideTitle: {
    marginVertical: 15,
    alignSelf: 'center',
  },
  imageName: {
    "textAlign": "center",
    "fontSize": 13,
    "width": Metrics.vw * 40,
    "paddingLeft": 6,
    "paddingTop": 6
  },
  imagePrice: {
      "color": "#999",
      "fontSize": 12,
      "textAlign": "center",
      "paddingTop": 6
  },
  listContainer: {
    backgroundColor: '#fff',
    borderColor: '#eee',
    borderBottomWidth: 1,
    flexDirection: 'row',
    padding: 5,
    height: Metrics.vh * 16,
  },
  listContent: {
    flex: 2.5,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    marginHorizontal: 5,
    marginVertical: 8
  },
  listBody: {
    flex: 2,
  },
  listAction: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 8,
    backgroundColor: 'transparent'
  },
  listImage: {
    flex: 1,
    flexGrow: 1,
    marginTop: 12,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 8,
    borderRadius: 2
  },
  listTitle: {
    fontSize: 14,
    marginRight: 8,
    marginTop: 4,
    color: '#333',
    fontWeight: '400'
  },
  listTitleDisabled: {
    fontSize: 14,
    marginRight: 8,
    marginTop: 4,
    color: Colors.themeAccent,
    fontWeight: '400'
  },
  listSubtitle: {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '300',
  },
  listDate: {
    color: "#999",
    fontSize: 11,
    marginTop: 6
  },
  chatInputContainer: {
    flexGrow: 0,
    flexDirection: 'row',
    alignItems: 'stretch',
    backgroundColor: 'white',
  },
  "total": {
    "flexDirection": "row",
    "paddingTop": 10,
    "paddingRight": 20,
    "paddingBottom": 0,
    "paddingLeft": 20,
    "borderTopWidth": 1,
    "borderTopColor": "#ddd",
    "width": Metrics.deviceWidth
  },
  "totalText": {
    "color": "#999",
    "fontSize": 12,
    "paddingTop": 6,
    "justifyContent": "center",
    "width": Metrics.deviceWidth/2 - 10
  },
  "totalPrice": {
    "color": "#333",
    "alignItems": "center",
    "textAlign": "right",
    "paddingRight": 30,
    "fontWeight": "600",
    "width": Metrics.deviceWidth/2
  },
  "totalPriceAndroid": {
    "color": "#333",
    "alignItems": "center",
    "textAlign": "right",
    "paddingRight": 60,
    "fontWeight": "600",
    "width": Metrics.deviceWidth/2 - 30
  },
  buttonCheckout: {
    flex: 1,
    paddingVertical: 20,
    marginVertical: 14,
    marginHorizontal: 8,
    borderWidth: 0.8,
    borderColor: Colors.themeHighlight,
    backgroundColor: Colors.themeHighlight,
  },
  buttonUpdateCart: {
    flex: 1,
    paddingVertical: 20,
    marginVertical: 14,
    marginHorizontal: 8,
    borderWidth: 0.8,
    borderColor: Colors.themeDark,
    backgroundColor: Colors.themeDark,
  },
})
