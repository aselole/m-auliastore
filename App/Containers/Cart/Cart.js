import React, { Component } from 'react'
import { Platform, View, ScrollView, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from 'Theme'
import {
  ImageSwipe, ImageCarousel, ToolbarButton, Text,
  Loading, InfoScreen, ListFilter, ImageLoading,
  ImageFitLoading, TextJustify, ButtonIcon, ConfirmModal
} from 'Template'
import { ImageList, ItemCounter, ButtonRoundBlue } from 'Component'
import AppConfig from 'Config'
import { isPropsChanged } from 'Lib/CheckUtils'
import Parallax from 'Lib/react-native-parallax'
import UserAction from 'Redux/UserRedux'
import ListAction from 'Redux/ListRedux'
import TransactionAction from 'Redux/TransactionRedux'
import { convertToRupiah } from 'Lib/NumberUtils'
import update from 'immutability-helper'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class Cart extends Component {

  // it actually style it's own navigator, i.e. HomeStack on TabNavigation.js
  static navigationOptions = ({navigation}) => {
    const {params = {}} = navigation.state
    return {
      title: 'Keranjang Belanja',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor],
      headerRight:
        <ToolbarButton
          onPress={() => params.onShowCoupon()}
          image={Images.coupon}
          styleContainer={stylesCommon.toolbarRightContainer}
          styleContentContainer={{flex:0.65}}
        />
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      data: null,
      finalTotal: 0,        // up-to-date harga total
      checkedList: null,    // status konfirmasi/hapus tiap produk
      initCountList: null,  // pivot kuantitas tiap produk
      countList: null,      // up-to-date kuantitas tiap produl
      updateDisabled: true,
      checkoutDisabled: false,
      confirmCheckoutTitle: '',
      confirmCheckoutMsg: 'Lanjut Checkout?'
    }
  }

  componentDidMount() {
    this.props.navigation.setParams({
      onShowCoupon: this._onShowCoupon,
    })
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      if(nextProps.data.hasOwnProperty('cart')) {
        this.setState({
          data: nextProps.data['cart']
        }, () => {
          this._setupInitChecked()
          this._calculateInitTotalPrice()
          this._calculateInitProductCount()
        })
      }
    }
  }

  _loadData() {
    this.props.getCart()
  }

  _onListClicked(item) {
    this.props.onListItemClicked(item)
  }

  // untuk awal2, semua produk dikonfirmasi untuk dibeli
  _setupInitChecked() {
    this.setState({
      checkedList: _.fill(Array(this.state.data.length), true)
    })
  }

  // menghitung harga total
  _calculateInitTotalPrice() {
    if(_.isNil(this.state.data)) return
    let total = 0
    _.forIn(this.state.data, (value, key) => {
      total += parseInt(value.price) * parseInt(value.qty)
    })
    this.setState({ finalTotal: total })
  }

  // untuk awal2, semua produk disimpan kuantitas awalnya
  _calculateInitProductCount() {
    let tempCount = []
    this.state.data.map((item) => tempCount.push(item.qty))
    this.setState({
      initCountList: tempCount,
      countList: tempCount
    })
  }

  // jika user merubah konfirmasi untuk produk yang mau dibeli
  _onCheckedChanged(index) {
    this.setState({
      checkedList: update( this.state.checkedList, {$splice: [[ index, 1, !this.state.checkedList[index] ]]} )
    }, () => {
      this._onQuantityChanged(
        index,
        this.state.checkedList[index] ? this.state.countList[index] : 0
      )
    })
  }

  // jika kuantitas produk berubah
  _onQuantityChanged(index, newQty) {
    let diffQty = newQty - this.state.countList[index]
    let diffPrice = 0
    if(diffQty == 0) {  // jika produk di konfirmasi kembali utk dibeli
      diffPrice = parseInt(this.state.data[index]['price']) * parseInt(this.state.countList[index])
    } else {
      diffPrice = parseInt(this.state.data[index]['price']) * parseInt(diffQty)
    }
    if(!this.state.checkedList[index]) {  // jika produk di hapus, kuantitas tetap dijaga
      newQty = this.state.countList[index]
    }
    this.setState({
      finalTotal: this.state.finalTotal + diffPrice,
      countList: update(this.state.countList, { $splice: [[index, 1, newQty]] })
    }, () => {
      this.checkUpdateValidity()
      this._checkCheckoutValidity()
    })
  }

  // buka dialog input kode kupon
  _onShowCoupon() {

  }

  // simpan perubahan produk atau kuantitas produk yang akan dibeli
  _onUpdateCart() {
    let data = []
    this.state.data.map((item, index) => {
      if(item.qty !== this.state.countList[index])
        data.push({id_cart:item.id_cart, new_qty:this.state.countList[index]})
    })
    this.props.updateCart(data)
  }

  // konfirmasi checkout
  _onConfirmCheckout() {

  }

  // aktifkan tombol update jika ada perubahan konfirmasi/kuantitas salah satu produk
  checkUpdateValidity() {
    // cek apa ada perubahan kuantitas
    let isDiff = !_.isEmpty(
      _.differenceWith(
        this.state.initCountList,
        this.state.countList, 
        _.isEqual
      )
    )
    // cek apa ada perubahan konfirmasi produk
    let isDisabled = _.findIndex(this.state.checkedList, (item) => !item) >= 0 ? true : false

    if(isDiff || isDisabled) {  // jika ada perubahan kuantitas / konfirmasi, maka aktifkan tombol update
      this.setState({
        updateDisabled: false,
        confirmCheckoutMsg: 'Update Keranjang Belanja dan Lanjut Checkout?'
      })
    } else {
      this.setState({
        updateDisabled:true,
        confirmCheckoutMsg: 'Lanjut Checkout?'
      })
    }
  }

  // cek jangan sampai tidak ada produk yg dikonfirmasi
  _checkCheckoutValidity() {
    if(this.state.finalTotal === 0) {
      this.setState({ checkoutDisabled:true })
    } else {
      this.setState({ checkoutDisabled:false })
    }
  }

  _renderList = ({ item, index }) => {
    let isChecked = true
    if(!_.isNil(this.state.checkedList)) {
      isChecked = this.state.checkedList[index] ? true : false                  // jika produk dihapus, maka ubah ui
    }
    let isExistQuantity = true
    if(!_.isNil(this.state.countList)) {
      isExistQuantity = this.state.countList[index] !== '0' ? true : false      // jika kuantitas dikurangi sampai 0, maka ubah ui
    }
    return (
      <View
        style={styles.listContainer}
        onPress={() => this._onListClicked(item)}>
        <View style={{flex:0.3, alignSelf:'center'}}>
          <ImageFitLoading
            urlDefault={isChecked && isExistQuantity ? Images.checked : Images.unchecked}
            path={isChecked && isExistQuantity ? Images.checked : Images.unchecked}
            style={{flex:1, alignSelf:'center'}}
            onPress={() => this._onCheckedChanged(index)}
          />
        </View>

        <View style={styles.listImage}>
          <ImageFitLoading
            urlDefault={Images.no_photo}
            path={item.thumbnail_path} />
        </View>

        <View style={styles.listContent}>
          <View style={styles.listBody}>
            <Text
              style={styles.listDate}
              numberOfLines={2}>
              {item.nama_product}
            </Text>
            <Text
              style={isChecked && isExistQuantity ? styles.listTitle : styles.listTitleDisabled}
              numberOfLines={1}>
              @ {convertToRupiah(item.price)}
            </Text>
          </View>
        </View>

        <ItemCounter
          onValueChanged={(number) => this._onQuantityChanged(index, number)}
          styleContainer={{flex:0.5, marginRight:3}}
          initialNumber={item.qty}
          isEnabled={isChecked}
        />
      </View>
    )
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={{flex:10}}>
         <ScrollView
           style={styles.scrollContainer}
           contentContainerStyle={styles.scrollContainerContent}>
            <ListFilter
              data={this.state.data}
              errorInfo={this.props.error_info}
              errorTitle={this.props.error_title}
              isFetching={this.props.is_fetching}
              renderRow={this._renderList.bind(this)}
              keys='id_cart'
              loadingType='wave'
              onLoadData={() => this._loadData()}
            />
         </ScrollView>

         <View style={styles.total}>
           <Text style={styles.totalText}>TOTAL</Text>
           <Text style={(Platform.OS === 'ios' ? styles.totalPrice : styles.totalPriceAndroid)}>
             {convertToRupiah(this.state.finalTotal)}
           </Text>
         </View>
         <View style={{flexDirection:'row'}}>
           <ButtonIcon
             onPress={() => this._onUpdateCart()}
             style={styles.buttonUpdateCart}
             styleContainer={{flexDirection:'row'}}
             titleColor='#fff'
             title='Update'
             bgColor={Colors.themeDark}
             loadingBgColor={Colors.theme}
             isDisabled={this.state.updateDisabled} />
           <ButtonIcon
             onPress={() => this._onConfirmCheckout()}
             style={styles.buttonCheckout}
             styleContainer={{flexDirection:'row'}}
             bgColor={Colors.themeHighlight}
             loadingBgColor={Colors.theme}
             titleColor='#fff'
             title='Checkout'
             isDisabled={this.state.checkoutDisabled}
             isNeedConfirm={true}
             confirmTitle={this.state.confirmCheckoutTitle}
             confirmMessage={this.state.confirmCheckoutMsg}
             confirmIcon={Images.info}
             styleButtonOk={{backgroundColor:Colors.themeHighlight}}
             styleButtonCancel={{backgroundColor:Colors.themeAccent}}
            />
         </View>
       </View>
     </View>
   )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.list.data,
    is_fetching: state.list.is_fetching,
    error_info: state.list.error_info,
    error_title: state.list.error_title,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCart: () => dispatch(ListAction.getCart()),
    onListItemClicked: (data_selected) => dispatch(ListAction.onListItemClicked(data_selected)),
    updateCart: (data) => dispatch(TransactionAction.updateCart(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart)
