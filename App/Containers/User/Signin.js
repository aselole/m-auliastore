import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Images, Servers, Strings } from 'Theme'
import UserAction from 'Redux/UserRedux'
import { TextInputIcon, ButtonIcon, Text, InfoPopup } from 'Template'
import { isPropsChanged } from 'Lib/CheckUtils'
var _ = require('lodash')

// Styles
import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

class Signin extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      title: 'Signin',
      headerStyle: [stylesCommon.noShadow, stylesCommon.mainBackgroundColor],
    }
  }

  constructor (props) {
    super(props)
    this.state = {
      username: '', password: '',
      error: false, error_info: ''
    }
  }

  componentDidMount() {
    this.props.reset()
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.response, nextProps.response)) {
      this.props.navigation.navigate('Home')
    }
  }

  _onSignin() {
    this.props.signin(this.state.username, this.state.password)
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <TextInputIcon
            placeholder='Username'
            image={Images.user}
            error={this.state.error_username}
            onChange={(username) => this.setState({username})}
            returnKeyType = {"next"}/>
          <TextInputIcon
            placeholder='Password'
            image={Images.password}
            error={this.state.error_password}
            onChange={(password) => this.setState({password})}
            secureTextEntry={true}/>
          <ButtonIcon
            isLoading={this.props.is_fetching}
            title='Enter'
            onPress={() => this._onSignin()}
            bgColor="rgba(60,135,217,0.8)"
            titleColor='#fff'
            style={{flexGrow:1}}
            styleContainer={{marginTop:18, flexDirection:'row'}} />
        </View>
        <InfoPopup
          isVisible={_.isEmpty(this.props.error_info) ? false : true}
          message={this.props.error_info}
          styleContainer={{flex:0.3}}
          icon={Images.fail}
        />
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    is_fetching: state.user.is_fetching,
    error_info: state.user.error_info,
    response: state.user.data_user,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    reset: () => dispatch(UserAction.reset()),
    signin: (username, password) => dispatch(UserAction.signin(username, password)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signin)
