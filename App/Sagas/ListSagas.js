import { call, put } from 'redux-saga/effects'
import ListAction from 'Redux/ListRedux'

export function * getCarousel (api, action) {
  const response = yield call(api.getCarousel, action)

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'carousel'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getBestSellerHome (api, action) {
  const response = yield call(api.getBestSellerHome, {'limit':'[4]'})

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'best_seller'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getNewArrivalHome (api, action) {
  const response = yield call(api.getNewArrivalHome, {'limit':'[4]'})

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'new_arrival'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getMostReviewHome (api, action) {
  const response = yield call(api.getMostReviewHome, {'limit':'[4]'})

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'most_review'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getDetailProduk (api, action) {
  const { kode } = action
  const response = yield call(api.getDetailProduk, {'where':'{"_.kode_product":"'+kode+'"}'})

  if(response.ok) {
    yield put(ListAction.onDetailReceived(response.data))
  } else {
    yield put(ListAction.onDetailError(response))
  }
}

export function * getListBerita (api, action) {
  const { start, length } = action
  const response = yield call(
    api.getListBerita,
    {
      'order':'[["_.created_at","desc"]]',
      'limit':'['+start+','+length+']'
    }
  )

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'artikel'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getCommentBerita (api, action) {
  const { id_newsfeed, id_parent } = action
  const response = yield call(
    api.getCommentBerita,
    {
      'where':'{"_.id_newsfeed":"'+id_newsfeed+'","_.id_parent":'+id_parent+'}'
    }
  )

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'comment'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}

export function * getCart (api, action) {
  const response = yield call(api.getCart, 'refresh-cart')

  if(response.ok) {
    yield put(ListAction.onDataReceived(response.data, 'cart'))
  } else {
    yield put(ListAction.onDataError(response))
  }
}
