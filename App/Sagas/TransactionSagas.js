import { call, put } from 'redux-saga/effects'
import TransactionAction from 'Redux/TransactionRedux'
var _ = require('lodash')

export function * postCommentBerita (api, action) {
  const { content, id_parent, id_newsfeed } = action
  const response = yield call(
    api.postCommentBerita,
    {
      'content': content,
      'id_parent': id_parent,
      'id_newsfeed': id_newsfeed
    }
  )

  if(response.ok) {
    yield put(TransactionAction.onTransactionReceived(response.data))
  } else {
    yield put(TransactionAction.onTransactionError(response))
  }
}

export function * updateCart (api, action) {
  const { data } = action

  let params = ''
  data.map((item) => {
    params += '{"id_cart":"'+item.id_cart+'","new_qty":'+item.new_qty+'},'
  })
  params = _.trimEnd(params, ',')

  const response = yield call(
    api.updateCart,
    {
      'carts': '['+params+']'
    }
  )

  if(response.ok) {
    yield put(TransactionAction.onTransactionReceived(response.data))
  } else {
    yield put(TransactionAction.onTransactionError(response))
  }
}
