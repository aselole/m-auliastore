import { takeLatest, takeEvery, all } from 'redux-saga/effects'
import API from 'Services/Api'
import DebugConfig from 'Config/DebugConfig'

/* ------------- Types ------------- */

import { UserTypes } from 'Redux/UserRedux'
import { ListTypes } from 'Redux/ListRedux'
import { TransactionTypes } from 'Redux/TransactionRedux'

/* ------------- Sagas ------------- */

import { signin, signup, isSignedIn } from './UserSagas'
import {
  getCarousel, getBestSellerHome, getNewArrivalHome,
  getMostReviewHome, getDetailProduk, getListBerita,
  getCommentBerita, getCart
} from './ListSagas'
import { postCommentBerita, updateCart } from './TransactionSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = API.create()

/* ------------- Connect Types To Sagas ------------- */

//root
export default function * root() {
  try {
    yield [
      // user
      takeLatest(UserTypes.SIGNIN, signin, api),
      takeLatest(UserTypes.SIGNUP, signup, api),
      takeEvery(UserTypes.IS_SIGNED_IN, isSignedIn, api),

      takeEvery(ListTypes.GET_CAROUSEL, getCarousel, api),
      takeEvery(ListTypes.GET_BEST_SELLER_HOME, getBestSellerHome, api),
      takeEvery(ListTypes.GET_NEW_ARRIVAL_HOME, getNewArrivalHome, api),
      takeEvery(ListTypes.GET_MOST_REVIEW_HOME, getMostReviewHome, api),

      takeEvery(ListTypes.GET_DETAIL_PRODUK, getDetailProduk, api),

      takeEvery(ListTypes.GET_LIST_BERITA, getListBerita, api),
      takeEvery(ListTypes.GET_COMMENT_BERITA, getCommentBerita, api),
      takeEvery(ListTypes.GET_CART, getCart, api),

      takeEvery(TransactionTypes.POST_COMMENT_BERITA, postCommentBerita, api),
      takeEvery(TransactionTypes.UPDATE_CART, updateCart, api),
    ]
  }
  catch(error) {
    console.tron.log('error saga '+error)
  }
}
