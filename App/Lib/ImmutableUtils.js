
import update from 'immutability-helper'

export function updateNestedRedux(nestedObject, index, val1, val2, val3) {
  return nestedObject.updateIn([index], (item) => {
    return item.merge({
        fetching: val1,
        loaded: val2,
        success: val3
    });
  });
}

export function updateNestedDataRedux(nestedObject, index, val1) {
  return nestedObject.updateIn([index], (item) => {
    return item.merge({
        value: val1
    });
  });
}

// push a value of immutable array
export function pushArray(items, newItem) {
  return items.concat(newItem)
}

// update a value of immutable array based on index
export function updateArray(items, index, newItem) {
  let temp = items.slice() //copy the array
  temp[index] = newItem
  return temp
}

// concat array to immutable array based on index
export function updateArrayWithArray(items, index, newArray) {
  let temp = items.slice() //copy the array
  newArray.map((item) => {
    temp[index++] = item
  })
  return temp
}

// delete first value of immmutable array
export function popArray(items, index) {
  return items.slice(1, items.length)
}

// delete a values of immmutable array based on index
export function deleteArray(items, index) {
  return items.slice(0,index).concat(items.slice(index+1))
}

// delete value of immmutable array based on index
export function deleteMultipleArrayValue(items, indexes) {
  let temp = items.slice()
  let _ = require('lodash')
  let output = _.pullAt(temp, indexes)
  return temp
}

// initialize/create immutable array from another array
export function initWithExistingArray(items) {
  let output = []
  items.map((item) => {
    output.push(item)
  })
  return output
}

// initialize/create immutable array based on a uniform boolean value
export function initWithBool(items, bool) {
  let output = []
  items.map((item) => {
    output.push(bool)
  })
  return output
}
