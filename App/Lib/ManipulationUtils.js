var _ = require('lodash')

export function getNormalizeEnum(data) {
  return Object.keys(data).map(value => {
    return {
      value,
      text: data[value]
    };
  });
}

export function getEnumFromArrayObject(data) {
  var output = {};
  for (let i = 0; i < data.length; i++) {
     row = data[i];
     output[ row.ID ] = row.NAMA;
  }
  return output;
}

export function arrayToObject(input) {
  var result = {};
  for (var i=0; i<arr.length; i++) {
    result[arr[i].key] = arr[i].value;
  }
  return result;
}

export function arrayToObjectES6(input) {
  var newObj = Object.assign({}, ...input);
  return newObj;
}

export function arrayToString(input) {
  let output = ''
  input.map((item) => {
    output += item + ','
  })
  return output
}

/*
* konversi objects of object ke array of object untuk options pada drodpown
* contoh: {{1:test},{2:test2}} => [{ID:1,NAMA:test},{ID:2,NAMA:test2}]
*/
export function objectToArrayObject(input, keyTitle, valueTitle) {
  if(keyTitle === undefined) keyTitle = 'ID'
  if(valueTitle === undefined) valueTitle = 'NAMA'
  let output = []
  Object.keys(input).map(key => {
    output.push({ [keyTitle]: key, [valueTitle]: input[key] })
  })
  return output
}

export function filterArrayObject(data, filter) {
  let output = data.filter(function(item) {
     return item.ID_RS_BASE == filter;
  }).map(function(item){
      delete item.ID_RS_BASE;
      return item;
  });
  return output;
}

export function getNormalizedWord(data) {
  if(data == null) return '-'
  else {
    return data.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    })
  }
}

export function isObject(input) {
  return input instanceof Object
}

export function stringToArray(input) {
  if(input == null || typeof input === 'undefined') return input
  let temp = new Array()
  let output = new Array()
  temp = input.split(",").filter(function(el) {return el.length != 0})
  temp.map((data, index) => {
    output.push(data)
  })
  return output
}

export function stringToPrefixArray(input, prefix) {
  if(input == null || typeof input === 'undefined') return input
  let temp = new Array()
  let output = new Array()
  temp = input.split(",").filter(function(el) {return el.length != 0})
  temp.map((data, index) => {
    output.push(prefix+data)
  })
  return output
}

export function removeLastComma(input) {
  if(input == null || typeof input === 'undefined') return input
  return input.replace(/,\s*$/, "");
}

// mennganti semua karakter spesial (e.g. /,*,^)
export function replaceSpecialChar(input, replacement) {
  let out1 = _.replace(input, new RegExp("[^a-zA-Z0-9]","gi"), " ")
  return _.replace(out1, new RegExp("\\s+","g"), replacement)
}
