var _ = require('lodash')

// cek apa props dan nextProps berbeda
export function isPropsChanged(item, nextItem) {
  if(!_.isEqual(item, nextItem) && !_.isNil(nextItem)) {
    return true
  } else {
    return false
  }
}

// cek apa input termasuk file dari storage lokal di hp
export function isLocalImage(input) {
  return input.includes('file:///')
}

// cek apa input termasuk image asset
export function isAssetImage(input) {
  if(_.isNumber(input)) return true
  else return false
}

export function isServerImage(input) {
  return input.includes('http://')
}

export function isObject(input) {
  return (Object.prototype.toString.call(input) === '[object Object]')
}

export function isString(input) {
  return (Object.prototype.toString.call(input) === '[object String]')
}

export function isArray(input) {
  return (Object.prototype.toString.call(input) === '[object Array]')
}

export function getType(input) {
  return Object.prototype.toString.call(input)
}
