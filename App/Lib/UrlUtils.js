
export function isURL(str) {
  let pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i') // fragment locator
  return pattern.test(str)
}

export function isURLContainImage(str) {
  let pattern = new RegExp('^https?://(?:[a-z0-9\-]+\.)+[a-z]{2,6}(?:/[^/#?]+)+\.(?:jpg|jpeg|gif|png)$','g')
  return pattern.test(str)
}

export function getFirstParam(param) {
  let index = param.indexOf(',')
  if(index > -1) {
    return param.slice(0, index)
  }
  return param
}

export function isFullUrl() {

}
