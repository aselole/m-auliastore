
export function getPercentage(current, total) {
  return Math.ceil( ((current+1)/total) * 100 )
}

export function setMaxTrailingNumber(number, _max) {
  let max = (_max == null || _max === undefined ? 2 : _max)
  let multiply = (max === 0 ? 1 : max * 10)
  return (Math.round(number * multiply)/multiply).toFixed(max)
}

// konversi koordinat degrees ke tipe koordinat Degree Minute Second
export function locationDegToDMS(lat, lng) {
  let latSeconds = Math.round(lat * 3600);
  let latDegrees = latSeconds / 3600;
  latSeconds = Math.abs(latSeconds % 3600);
  let latMinutes = latSeconds / 60;
  latSeconds %= 60;

  let longSeconds = Math.round(lng * 3600);
  let longDegrees = longSeconds / 3600;
  longSeconds = Math.abs(longSeconds % 3600);
  let longMinutes = longSeconds / 60;
  longSeconds %= 60;
  let latDegree = latDegrees >= 0 ? "N" : "S";
  let lonDegrees = longDegrees >= 0 ? "E" : "W";

  let lintang = Math.abs(setMaxTrailingNumber(latDegrees,0)) + "°" + setMaxTrailingNumber(latMinutes,0) + "'" + setMaxTrailingNumber(latSeconds,0) + "\"" + latDegree
  let bujur = Math.abs(setMaxTrailingNumber(longDegrees,0)) + "°" + setMaxTrailingNumber(longMinutes,0) + "'" + setMaxTrailingNumber(longSeconds,0) + "\"" + lonDegrees
  return { lintang: lintang, bujur: bujur }
}

// konversi koordinat degrees ke tipe koordinat UTM
export function locationDegToUTM(lat, lng) {
  let resultEasting;
  let resultNorthing;
  let zone;
  let letter;
  zone = Math.floor(lng/6+31);
  if (lat<-72)
      letter='C';
  else if (lat<-64)
      letter='D';
  else if (lat<-56)
      letter='E';
  else if (lat<-48)
      letter='F';
  else if (lat<-40)
      letter='G';
  else if (lat<-32)
      letter='H';
  else if (lat<-24)
      letter='J';
  else if (lat<-16)
      letter='K';
  else if (lat<-8)
      letter='L';
  else if (lat<0)
      letter='M';
  else if (lat<8)
      letter='N';
  else if (lat<16)
      letter='P';
  else if (lat<24)
      letter='Q';
  else if (lat<32)
      letter='R';
  else if (lat<40)
      letter='S';
  else if (lat<48)
      letter='T';
  else if (lat<56)
      letter='U';
  else if (lat<64)
      letter='V';
  else if (lat<72)
      letter='W';
  else
      letter='X';
  resultEasting = 0.5*Math.log((1+Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*zone-183)*Math.PI/180))/(1-Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*zone-183)*Math.PI/180)))*0.9996*6399593.62/Math.pow((1+Math.pow(0.0820944379, 2)*Math.pow(Math.cos(lat*Math.PI/180), 2)), 0.5)*(1+ Math.pow(0.0820944379,2)/2*Math.pow((0.5*Math.log((1+Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*zone-183)*Math.PI/180))/(1-Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*zone-183)*Math.PI/180)))),2)*Math.pow(Math.cos(lat*Math.PI/180),2)/3)+500000;
  resultEasting = Math.round(resultEasting*100)*0.01;
  resultNorthing = (Math.atan(Math.tan(lat*Math.PI/180)/Math.cos((lng*Math.PI/180-(6*zone -183)*Math.PI/180)))-lat*Math.PI/180)*0.9996*6399593.625/Math.sqrt(1+0.006739496742*Math.pow(Math.cos(lat*Math.PI/180),2))*(1+0.006739496742/2*Math.pow(0.5*Math.log((1+Math.cos(lat*Math.PI/180)*Math.sin((lng*Math.PI/180-(6*zone -183)*Math.PI/180)))/(1-Math.cos(lat*Math.PI/180)*Math.sin((lng*Math.PI/180-(6*zone -183)*Math.PI/180)))),2)*Math.pow(Math.cos(lat*Math.PI/180),2))+0.9996*6399593.625*(lat*Math.PI/180-0.005054622556*(lat*Math.PI/180+Math.sin(2*lat*Math.PI/180)/2)+4.258201531e-05*(3*(lat*Math.PI/180+Math.sin(2*lat*Math.PI/180)/2)+Math.sin(2*lat*Math.PI/180)*Math.pow(Math.cos(lat*Math.PI/180),2))/4-1.674057895e-07*(5*(3*(lat*Math.PI/180+Math.sin(2*lat*Math.PI/180)/2)+Math.sin(2*lat*Math.PI/180)*Math.pow(Math.cos(lat*Math.PI/180),2))/4+Math.sin(2*lat*Math.PI/180)*Math.pow(Math.cos(lat*Math.PI/180),2)*Math.pow(Math.cos(lat*Math.PI/180),2))/3);
  if (letter<'M') resultNorthing = resultNorthing + 10000000;
  resultNorthing=Math.round(resultNorthing*100)*0.01;
  return setMaxTrailingNumber(resultEasting,2) + "m E " + setMaxTrailingNumber(resultNorthing,2) + "m N";
}

/**
 * Usage example:
 * alert(convertToRupiah(10000000)); -> "Rp. 10.000.000"
 */
export function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
	return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
}
