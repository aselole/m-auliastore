import apisauce from 'apisauce'
import { jsonToFormEncodedCaps } from 'Lib/JsonUtils'
import AppConfig from 'Config/AppConfig'
import { store } from 'Containers/App'       // how to access redux from non-react component
var _ = require('lodash')

const create = (baseURL = AppConfig.baseServerURL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    },
    timeout: 30000,
    onUploadProgress: progress => {                 // listener untuk progress upload attachment
      // store.dispatch(UploadActions.dataUploadProgress(progress.loaded, progress.total, store.getState().upload.upload_sequence[0]))   // how to access redux from non-react component
    }
  })

  api.addRequestTransform(request => {  // rubah format request parameter
    request.data = jsonToFormEncodedCaps(request.data)
    if(!_.isNil(request.data))
      request.data = request.data.replace('%3A', ':')
  })

  api.addResponseTransform(response => {  // rubah format response parameter
    if(_.isNil(response.data) || _.isEmpty(response.data)) response.ok = false
    else if(response.data.code === 500) response.ok = false
  })

  const signin = (payload) => api.post('sim/login/do_login_json', payload)
  const isSignedIn = (payload) => api.post('sim/login/is_logged_in', payload)
  const signup = (payload) => api.post('UserGobang/signup', payload)

  const getCarousel = (payload) => api.get('ecp_home/carousel/get_many?get-carousel')
  const getBestSellerHome = (payload) => api.post('ecp_product/best_seller/get_many?best-seller-home', payload)
  const getNewArrivalHome = (payload) => api.post('ecp_product/new_arrival/get_many?new-arrival-home', payload)
  const getMostReviewHome = (payload) => api.post('ecp_product/most_review/get_many?most-review-home', payload)

  const getDetailProduk = (payload) => api.post('ecp_home/product/get_one?get-detail-produk', payload)

  const getListBerita = (payload) => api.post('ecp_article/article/get_many?get-article', payload)
  const getCommentBerita = (payload) => api.post('ecp_article/article_comments/get_many?get-comment', payload)
  const postCommentBerita = (payload) => api.post('ecp_article/comments/save_json', payload)

  const getCart = (payload) => api.get('ecp_shopping/cart/get_many?refresh-cart', payload)
  const updateCart = (payload) => api.post('ecp_shopping/cart/update_json?update-cart', payload)

  return {
    signin,
    signup,
    isSignedIn,
    getCarousel,
    getBestSellerHome,
    getNewArrivalHome,
    getMostReviewHome,
    getDetailProduk,
    getListBerita,
    getCommentBerita,
    postCommentBerita,
    getCart,
    updateCart,
  }
}

export default {
  create
}
