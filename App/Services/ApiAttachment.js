import apisauce from 'apisauce'
import { jsonToFormEncodedCaps } from 'Lib/JsonUtils'
import AppConfig from 'Config/AppConfig'
import UploadActions from 'Redux/UploadRedux'
import { store } from 'Containers/App'       // how to access redux from non-react component

// API untuk handle attachment upload
const create = (baseURL = AppConfig.baseServerURL) => {
  const apiAttachment = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Content-Type': 'multipart/form-data',
      'Accept': 'application/json'
    },
    timeout: 30000,
    onUploadProgress: progress => {                 // listener untuk progress upload attachment
      // store.dispatch(UploadActions.attachmentUploadProgress(progress.loaded, progress.total, store.getState().upload.upload_sequence[0]))   // how to access redux from non-react component
    }
  })

  apiAttachment.addRequestTransform(request => {  // rubah format request parameter
  })

  apiAttachment.addResponseTransform(response => {  // rubah format response parameter
    if(response.data == null) response.ok = false
    else if(response.data.code === 200) response.ok = true
    else response.ok = false
  })

  // attachment upload
  const uploadImage = (payload) => apiAttachment.post('Survey/uploadAttachment', payload)

  return {
    uploadImage
  }
}

export default {
  create
}
