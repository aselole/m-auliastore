import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, Dimensions, StyleSheet, View, Text,
  Image, SectionList, ScrollView, TouchableOpacity
} from 'react-native'
import { Colors, Strings, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import { DotIndicator, WaveIndicator, PacmanIndicator } from 'react-native-indicators'
import update from 'immutability-helper'
import { filterData } from 'Lib/FilterUtils'
import InfoModal from './InfoModal'
import ImageFitLoading from './ImageFitLoading'
import Loading from './Loading'
import InfoScreen from './InfoScreen'
import Parallax from 'Lib/react-native-parallax'
import AppConfig from 'Config/AppConfig'
var _ = require('lodash')

/*
 * List data dengan fitur filter
 * metode filter: otomatis filter ketika ada perubahan kata kunci pencarian/parameter)
*/
export default class ListFilter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: null,
      is_empty: false,
      error_title: null,
      collapse_error: false,
      batch_start_index: 0,
      batch_processing: false,
      batch_fetching: false,
    }
  }

  componentDidMount() {
    this._requestData()
    this._initData()
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.errorTitle, nextProps.errorTitle) && !_.isNull(nextProps.errorTitle)) {
      this.setState({
        error_title: nextProps.errorTitle,
        batch_fetching: false   // remove loading screen on below of list
      })
    }
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNull(nextProps.data)) {
      this._populateList(nextProps.data)
      this.setState({ batch_fetching: false })  // remove loading screen on below of list
    }
    if(!_.isEqual(this.props.filterParam, nextProps.filterParam) && !_.isNull(nextProps.filterParam)) {
      this._performFilter(nextProps.filterParam)
    }
  }

  // request api to fetch data
  _requestData() {
    if(this.props.isLoadBatch) {
      this.props.onLoadData(0, this.props.batchSize)   // jika per-batch, maka kasih parameter ukuran batch
    } else {
      this.props.onLoadData()   // jika bukan load data per-batch, maka tidak usah kasih parameter ukuran batch
    }
  }

  // fetch next data batch from api
  _onLoadNextBatch() {
    if( !this.props.isLoadBatch ||    // tidak perlu load batch berikutnya, jika: bukan mode batching,
        this.props.isFetching ||      // jika sedang proses fetching
        !_.isNil(this.props.errorTitle)  // jika data batch yg diterima kosong atau error
      ) return
    this.setState((prevState) => {
      return {
        batch_processing: true, // indicate on batch processing mode
        batch_fetching: true,   // show loading screen on below of list
        batch_start_index: prevState.batch_start_index + this.props.batchSize
      }
    }, () => {
      this.props.onLoadData(
        this.state.batch_start_index,
        this.props.batchSize
      )
    })
  }

  // if data exist since beginning, thus setup data
  _initData() {
    if(_.isNil(this.props.data)) return
    this.setState({ items: this._normalizeData(this.props.data) })
  }

  // ada update data baru
  _populateList(data) {
    if(_.isArray(data) && !_.isEmpty(data)) {
      this.setState({
        items: this._normalizeData(data)
      }, () => {
        if(this.props.isScrollOnRefresh && this._list) {  // fitur refresh data, terus arahkan view ke data baru
          let wait = new Promise((resolve) => setTimeout(resolve, 500));  // Smaller number should work
          wait.then( () => {
            this._list.scrollToLocation({
              itemIndex: this.state.items[0]['data'][this.state.items[0]['data'].length-1].id,
            })
          });
        }
      })
    } else {
      this.setState({
        is_empty: true,
        error_title: 'Data Kosong'
      })
    }
  }

  // benerin struktur data source yang akan dipakai sectionlist atau parallax
  _normalizeData(data) {
    if(!this.state.batch_processing) {  // jika bukan load per-batch, overwrite data
      if(this.props.type === 'section') {
        if(!this.props.isMultiSection) return [{ data: data }]
        else return data
      } else {
        return data
      }
    } else {  // jika load per-batch, append data
      if(this.props.type === 'section') {
        return update(this.state.items, {0: {data: {$push: data}}})
      } else {
        return update(this.state.items, {$push: data})
      }
    }
  }

  // pencarian data
  _performFilter(filters) {
    this.setState({
      items: this._normalizeData(this.props.data)
    }, () => {
      let newData =
      ( this.state.items !== null &&
          this.state.items[0].data.filter((item) =>
            filters.every((filter) => {
              if(filter.type === 'dropdown' && filter.value == 'Semua') {         // utk parameter tipe dropdown, jika pilih 'semua' maka tdk perlu filter
                return true
              } else {                                                            // utk parameter tipe lain, periksa apa cocok dgn current data atau tidak
                return item[filter.column].toLowerCase().includes(filter.value.toLowerCase())
              }
            }
        ))
      )
      this.setState(prevState => ({
        items: update(prevState.items, {0: {data: {$set: newData}}})
      }))
    })
  }

  _onParallaxClicked(item) {
    this.props.onItemClicked(item)
  }

  // is fetching or batching finished and success fetching data?
  _isShowList(type) {
    if(
        (!_.isNull(this.state.items) &&
        !this.props.isFetching &&
        this.props.type === type) ||
        this.state.batch_processing   // if in batching progress, keep showing loading screen
      ) return true
    else return false
  }

  // need to show loading screen?
  _isShowLoading() {
    if(
        this.props.isFetching &&
        !this.state.batch_processing  // if is not in batching progress, no need to show loading screen
      ) return true
    else return false
  }

  // need to show error screen?
  _isShowError() {
    if(
        !this.props.isFetching &&
        (!_.isNull(this.state.error_title) || this.state.is_empty)
      ) return true
    else return false
  }

  // show loading ui on below of list
  _renderBatchLoading = () => {
    if(this.state.batch_fetching) {
      return (
        <Loading
          isInfinite={true}
          infiniteLoadingType='dot'
          styleContainer={styles.batchLoading}
          loadingSize={10}
        />
      )
    } else {
      return null
    }
  }

  render () {
    if(this._isShowList('section')) {
      return (
        <SectionList
          ref={(ref) => this._list = ref}
          sections={this.state.items}
          renderItem={this.props.renderRow}
          renderSectionHeader={this.props.renderHeader}
          keyExtractor={(item, index) => _.isNull(this.props.keys) ? item.id : item[this.props.keys] }
          horizontal={this.props.isHorizontal}
          style={this.props.style}
          contentContainerStyle={this.props.styleContent}
          showsVerticalScrollIndicator={this.props.isShowScroll}
          onEndReached={() => this._onLoadNextBatch()}
          ListFooterComponent={this._renderBatchLoading}
        />
      )
    } else if(this._isShowList('parallax')) {
      return (
        <Parallax.ScrollView style={{flex:1}}>
          {
            this.state.items.map((item, index) => (
              <Parallax.Image
               key={index}
               style={styles.parallaxImage}
               onPress={() => this._onParallaxClicked(item)}
               overlayStyle={styles.parallaxOverlay}
               source={{ uri: this.props.parallaxBaseImageUri + item[this.props.parallaxImageKey]}}
               parallaxFactor={0.8}>
                {
                  !_.isNil(this.props.parallaxTitleKey) &&
                    <Text style={styles.parallaxTitle}>{item[this.props.parallaxTitleKey]}</Text>
                }
                {
                  !_.isNil(this.props.parallaxSubtitleKey) &&
                    <Text style={styles.parallaxSubtitle}>{item[this.props.parallaxSubtitleKey]}</Text>
                }
              </Parallax.Image>
            ))
          }
        </Parallax.ScrollView>
      )
    } else if(this._isShowLoading()) {
      return (
        <Loading
          isInfinite={true}
          infiniteLoadingType={this.props.loadingType}
          styleContainer={{alignItems:'center', justifyContent:'center', alignSelf:'center'}}
        />
      )
    } else if(this._isShowError()) {
      return (
        <InfoScreen
          icon={this.state.is_empty ? Images.info : Images.fail}
          title={this.state.error_title}
          body={this.props.errorInfo}
        />
      )
    } else {
      return (null)
    }
  }
}

const styles = StyleSheet.create({
  parallaxImage: {
    "width": Metrics.deviceWidth,
    "height": 200,
    "borderBottomWidth": 1,
    "borderBottomColor": "#fff"
  },
  parallaxOverlay: {
    "alignItems": "center",
    "justifyContent": "center",
    "backgroundColor": "rgba(0,0,0,0.3)"
  },
  parallaxTitle: {
    "fontSize": 18,
    "textAlign": "center",
    "lineHeight": 25,
    "fontWeight": "400",
    "color": "white",
    "shadowOffset": {width: 0, height: 0},
    "shadowRadius": 1,
    "shadowColor": "black",
    "shadowOpacity": 0.8,
    "fontFamily": 'Helvetica'
  },
  parallaxSubtitle: {
    "opacity": 0.9,
    "fontSize": 13,
    "textAlign": "center",
    "color": "white",
    "paddingTop": 6
  },
  batchLoading: {
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 0,
    shadowOpacity: 0,
    paddingVertical: 20
  }
});

ListFilter.propTypes = {
  /*
  * isMultiSection is false => [{},{},...,{}]
  * isMultiSection is true =>
  * {[
  *   {title: 'Title1', data: ['item1', 'item2']},
  *   {title: 'Title2', data: ['item3', 'item4']},
  *   {title: 'Title3', data: ['item5', 'item6']},
  * ]}
  *
  * khusus parallax:
  * data [{..., uri:xxx},{..., uri:xxx}] => uri is background image source
  */
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  onLoadData: PropTypes.func.isRequired,     // fungsi yang dipanngil untuk download data
  /*
   * tipe tampilan list: section atau parallax
  */
  type: PropTypes.string,
  isFetching: PropTypes.bool,
  isDataEmpty: PropTypes.bool,
  errorTitle: PropTypes.oneOfType([ PropTypes.string, PropTypes.object ]),
  errorInfo: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.array ]),
  renderRow: PropTypes.func,
  renderHeader: PropTypes.func,
  keys: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object ]),
  /*
   * parameter pencarian data yg fleksibel
   * update state ini di parent utk filter data scr otomatis
   * format filter => [{column: 'JUDUL OBJECT YG DICARI', type: 'text/dropdown'. value: 'APA YANG DICARI'}]
  */
  filterParam: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  isHorizontal: PropTypes.bool,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  styleContent: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  isScrollOnRefresh: PropTypes.bool,
  isShowScroll: PropTypes.bool,
  isMultiSection: PropTypes.bool,
  loadingType: PropTypes.string,

  // fitur infinity load data batch
  isLoadBatch: PropTypes.bool,   // mengaktifkan atau menonaktifkan fitur infinity load data batch atau tidak
  //onRequestBatch: PropTypes.func,  // cantumkan fungsi utk download data
  batchSize: PropTypes.number, // banyaknya data per batch

  // parallax only
  onItemClicked: PropTypes.func,
  parallaxBaseImageUri: PropTypes.string, // background image base url
  parallaxImageKey: PropTypes.string, // identify on which image uri is stored on data object
  parallaxTitleKey: PropTypes.string, // identify on which title is stored on data object
  parallaxSubtitleKey: PropTypes.string, // identify on which sub-title is stored on data object
}

ListFilter.defaultProps = {
  type: 'section',
  keys: null,
  isHorizontal: false,
  isScrollOnRefresh: false,
  isMultiSection: false,
  loadingType: 'dot',
  parallaxImageKey: null,
  parallaxTitleKey: null,
  parallaxSubtitleKey: null,
  isShowScroll: false,
  isLoadBatch: false,
  batchSize: 10,
}
