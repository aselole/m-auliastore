import React, { Component } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView,
} from 'react-native'
import PropTypes from 'prop-types'
import { Colors, Images, Strings } from 'Theme'
import Modal from 'react-native-modal'
import { isPropsChanged } from 'Lib/CheckUtils'
import ImageFitLoading from './ImageFitLoading'
var _ = require('lodash')

export default class ConfirmModal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
    }
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.isVisible, nextProps.isVisible))
      this._onToggleModal()
  }

  _onToggleModal() {
    this.setState((prevState) => {
      return { isVisible: !prevState.isVisible }
    })
  }

  _onPressOk() {
    this._onToggleModal()
    if(this.props.hasOwnProperty('onPressOk')) {
      if(!_.isNil(this.props.onPressOk) && !_.isEmpty(this.props.onPressOk)) {
        this.props.onPressOk()
      }
    }
  }

  _onPressCancel() {
    this._onToggleModal()
    if(this.props.hasOwnProperty('onPressCancel')) {
      if(!_.isNil(this.props.onPressCancel) && !_.isEmpty(this.props.onPressCancel)) {
        this.props.onPressCancel()
      }
    }
  }

  render() {
    return (
      <Modal
        isVisible={this.state.isVisible}
        backdropColor='gray'
        backdropOpacity={0.5}
        animationInTiming={200}>
        <View style={styles.container}>

          <View style={styles.icon}>
            <ImageFitLoading
              urlDefault={this.props.icon}
              path={this.props.icon}
              style={styles.iconImage}
            />
          </View>

          <View style={styles.body}>
            { !_.isNil(this.props.title) && !_.isEmpty(this.props.title) &&
              <Text style={[ styles.title ]}>
                {this.props.title}
              </Text>
            }
            { !_.isNil(this.props.message) && !_.isEmpty(this.props.message) &&
              <Text style={[ styles.message ]}>
                {this.props.message}
              </Text>
            }
          </View>

          <View style={styles.buttonContainer}>
            <TouchableOpacity style={[styles.buttonDialog, this.props.styleButtonOk]}
              onPress={this._onPressOk.bind(this)}>
              <Text style={[ styles.buttonTitle ]}>
                {this.props.titleOk}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={[styles.buttonDialog, this.props.styleButtonCancel]}
              onPress={this._onPressCancel.bind(this)}>
              <Text style={[ styles.buttonTitle ]}>
                {this.props.titleCancel}
              </Text>
            </TouchableOpacity>
          </View>

        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.3,
    flexDirection: 'column',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    elevation: 12,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 8,
    shadowOpacity: 0.5,
  },
  icon: {
    flex: 1.5,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  iconImage: {
    flex: 0.8,
    alignSelf:'center',
  },
  title: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 16,
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
  },
  message: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 14,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  buttonDialog: {
    flex: 1,
    borderRadius: 20,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  buttonTitle: {
    textAlign: 'center',
    fontSize: 14,
    color: '#fff'
  }
});

ConfirmModal.propTypes = {
  isVisible: PropTypes.bool,
  onPressOk: PropTypes.func,
  onPressCancel: PropTypes.func,
  titleOk: PropTypes.string,
  titleCancel: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  icon: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  styleButtonOk: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleButtonCancel: PropTypes.oneOfType([ PropTypes.number, PropTypes.object, PropTypes.array ]),
};

ConfirmModal.defaultProps = {
  titleOk: 'OK',
  titleCancel: 'BATAL',
}
