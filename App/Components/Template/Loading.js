import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet, View, Text,
  Image, TextInput, TouchableHighlight, TouchableOpacity
} from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal'
import * as Progress from 'react-native-progress'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
var _ = require('lodash')

export default class Loading extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading_ui: null, progress: 0,
    }
  }

  componentDidMount() {
    this._setupLoadingUI()
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.progress, nextProps.progress)) {
      this.setState({ progress: nextProps.progress })
    }
  }

  _setupLoadingUI() {
    let loading = null
    if(this.props.isInfinite) {
      switch(this.props.infiniteLoadingType) {
        case 'wave':
          loading = <WaveIndicator color={this.props.color} count={this.props.loadingCount} size={this.props.loadingSize} />
          break
        default:
          loading = <DotIndicator color={this.props.color} count={this.props.loadingCount} size={this.props.loadingSize} />
          break
      }
    } else {
      loading =
        <Progress.Circle
          size={80}
          thickness={3}
          showsText={true}
          textStyle={[ styles.extraBold, {fontWeight:'bold', color:'red'} ]}
          strokeCap='round'
          color={this.props.color}
          progress={this.state.progress} />
    }
    this.setState({ loading_ui: loading })
  }

  render () {
    return (
      <View style={[ styles.container, this.props.styleContainer ]}>
        <View style={[ styles.loading, this.props.styleSubContainer ]}>
          {this.state.loading_ui}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor:'white',
    borderRadius: 15,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: 8
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  loading: {
    flex:0.3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'transparent'
  }
});

Loading.propTypes = {
  isInfinite: PropTypes.bool,
  infiniteLoadingType: PropTypes.string,
  progress: PropTypes.number,
  color: PropTypes.string,
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleSubContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  loadingSize: PropTypes.number,
  loadingCount: PropTypes.number,
}

Loading.defaultProps = {
  isInfinite: true,
  infiniteLoadingType: 'wave',
  color: 'red',
  loadingCount: 4,
  loadingSize: 80,
}
