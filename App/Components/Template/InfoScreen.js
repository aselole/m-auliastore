import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet, View, ScrollView,
  Image, TextInput, TouchableHighlight, TouchableOpacity
} from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Text from './Text'
import FlexImage from 'react-native-flex-image'
import CollapseView from 'react-native-collapse-view'
var _ = require('lodash')

export default class Loading extends Component {

  render () {
    return (
      <ScrollView
        style={[ styles.container, this.props.styleContainer]}
        contentContainerStyle={[ styles.contentContainer, this.props.styleContentContainer ]}>
        <View style={styles.imageContainer}>
          <FlexImage
            style={{flexGrow:0.4}}
            source={this.props.icon} />
        </View>
        <View style={styles.bodyContainer}>
          <CollapseView
            style={{flexGrow:1}}
            collapse={false}
            renderView={(collapse) => (
              <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text>{this.props.title}</Text>
                { !_.isEmpty(this.props.body) &&
                  <Image source={Images.down} style={{marginLeft:5, width:20, height:20}} />
                }
              </View>
            )}
            renderCollapseView={(collapse) => {
              if(_.isEmpty(this.props.body)) return (<View></View>)
              else return (
                <View style={{height:Metrics.deviceHeight, flexGrow:1, backgroundColor:'transparent'}}>
                  <Text style={{margin:10, padding:10, backgroundColor:'white'}}>{this.props.body}</Text>
                </View>
              )
            }}
          />
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent'
  },
  contentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  imageContainer: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  bodyContainer: {
    flexGrow: 7,
    marginTop: 10,
    paddingBottom: 10,
    backgroundColor: 'transparent'
  }
});

Loading.propTypes = {
  icon: PropTypes.number,
  title: PropTypes.string,
  body: PropTypes.string,
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleContentContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
}

Loading.defaultProps = {
  title: '',
  body: ''
}
