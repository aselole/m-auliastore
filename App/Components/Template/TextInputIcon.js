import React, {Component} from "react"
import {Text, View, TextInput, Image, StyleSheet} from "react-native"
import PropTypes from 'prop-types'
var _ = require('lodash')

export default class TextInputIcon extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.error, nextProps.error)) {
      this.setState({ error: nextProps.error })
    }
  }

  _onChangeText(text) {
    this.setState({ error: null })
    if(this.props.hasOwnProperty('onChange')) {
      this.props.onChange(text)
    }
  }

  render() {
    return (
      <View style={[ styles.buttonRound, this.props.styleContainer ]}>
        <Image source={this.props.image} style={[ styles.inputIcon, this.props.styleImage ]}></Image>
        <TextInput
            underlineColorAndroid='rgba(0,0,0,0)'
            style={[ styles.textInputDark, this.props.styleInput ]}
            placeholderTextColor={"#aaa"}
            onChangeText={(text) => this._onChangeText(text)}
            ref={(input) => this.props.inputRef && this.props.inputRef(input)}
            {...this.props} />
        { !_.isEmpty(this.state.error) &&
          <Text style={[this.props.styleError, styles.error]}>
            {this.state.error}
          </Text>
        }
      </View>
    )
  }

}

const styles = StyleSheet.create({
  error: {
    fontSize: 12,
    color:'red',
  },
  inputIcon: {
    position: "absolute",
    left: 0,
    top: 6,
    resizeMode: "contain",
    height: 24,
    width: 24
  },
  buttonRound: {
    position: "relative",
    borderColor: "#ddd",
    borderWidth: 0.8,
    borderTopWidth: 0,
    borderRightWidth: 0,
    borderLeftWidth: 0,
    marginTop: 10,
    marginRight: 8,
    marginBottom: 8,
    marginLeft: 8,
    paddingBottom: 8
  },
  textInputDark: {
    height: 40,
    backgroundColor: "transparent",
    color: "rgba(0,0,0,0.9)",
    paddingLeft: 40,
  },
})

TextInputIcon.propTypes = {
  image: PropTypes.oneOfType([ PropTypes.string, PropTypes.object, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleImage: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleInput: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleError: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  error: PropTypes.string,
  onChange: PropTypes.func,
};

TextInputIcon.defaultProps = {};
