import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types'
import LinearGradient from 'react-native-linear-gradient'
import { Colors, Images, Servers, Strings } from 'Theme'
import { Dialog } from 'react-native-simple-dialogs'
import Modal from 'react-native-modal'
import FlexImage from 'react-native-flex-image'
import ViewMoreText from 'react-native-view-more-text'
import Collapsible from 'react-native-collapsible'
var _ = require('lodash')

export default class InfoModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      flexContainer: 0.3,
      flexIconContainer: 1.5,
      flexMessageContainer: 0.1,
      flexButtonContainer: 0.7,
      title: null,
      body: null,
    }
  }

  componentDidMount() {
    this._parseTitle(this.props.message)
    this._parseBody(this.props.message)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.message, nextProps.message) && !_.isNull(nextProps.message)) {
      this._parseTitle(nextProps.message)
      this._parseBody(nextProps.message)
    }
  }

  // jika message sangat panjang, maka tinggi container menyesuaikan
  _onContentChanged(height) {
    if(height > 50) {
      this.setState({
        flexContainer: 1,
        flexIconContainer: 0.15,
        flexMessageContainer: 0.8,
        flexButtonContainer: 0.1,
      })
    }
  }

  _parseTitle(input) {
    let output = ''
    if(input) {
      if(input.hasOwnProperty('status')) {
        switch(input['status']) {
          case 200:
            output = '500 - ERROR'
            break
          default:
            output = input['status'] + ' - ' + input['problem']
            break
        }
      } else {
        output = 'ERROR'
      }
    } else {
      output = 'xxx - unidentified'
    }
    this.setState({ title: output })
  }

  _parseBody(input) {
    let output = ''
    if(input) {
      if(input.hasOwnProperty('data')) {
        let message = input['data']
        if(isObject(message)) output = ''
        else if(isString(message)) output = message
      }
    }
    this.setState({ body: output })
  }

  render() {
    return (
      <Modal
        isVisible={this.props.isVisible}
        backdropColor='gray'
        backdropOpacity={0.5}
        animationInTiming={1000}>
        <View style={[ styles.container, {flex: this.state.flexContainer} ]}>

          <View style={[ styles.iconContainer, {flex: this.state.flexIconContainer} ]}>
            <FlexImage source={this.props.icon} style={styles.icon} />
          </View>

          <ScrollView
            style={[ styles.messageContainer, {flex: this.state.flexMessageContainer} ]}
            contentContainerStyle={styles.messageSubContainer}
            onContentSizeChange={(w, h) => this._onContentChanged(h)}>
            { !_.isNull(this.state.title) && !_.isEmpty(this.state.title) &&
              <Text style={[{textAlign: 'center', fontSize: 16}]}>
                {this.state.title}
              </Text>
            }
            { !_.isNull(this.state.body) && !_.isEmpty(this.state.body) &&
              <Text style={[{textAlign: 'center', fontSize: 14}]}>
                {this.state.body}
              </Text>
            }
          </ScrollView>

          <View style={[ styles.buttonContainer, {flex: this.state.flexButtonContainer} ]}>
            <TouchableOpacity style={styles.button}
              onPress={this.props.onPressOk}>
              <Text style={[{textAlign: 'center', fontSize:16} ]}>
                OK
              </Text>
            </TouchableOpacity>
          </View>

        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    elevation: 12,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 8,
    shadowOpacity: 0.5,
  },
  iconContainer: {
    backgroundColor: 'transparent',
  },
  icon: {
    alignSelf: 'center',
    position: 'absolute',
    top: 0,
    bottom: 0,
  },
  messageContainer: {
    flex: 0.1,
    flexDirection:'column',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  messageSubContainer: {
    justifyContent:'center'
  },
  buttonContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  button: {
    flex: 1,
    backgroundColor: '#cccccc',
    borderRadius: 20,
    backgroundColor: Colors.theme,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 12,
    shadowOffset: {
      width: 2,
      height: 2,
    },
    shadowRadius: 4,
    shadowOpacity: 0.5,
  },
});

InfoModal.propTypes = {
  isVisible: PropTypes.bool,
  /*
  * struktur data message:
  *   object = {status:'', data:''}
  *   string = 'xxx'
  */
  message: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  onPressOk: PropTypes.func,
  icon: PropTypes.oneOfType([ PropTypes.number, PropTypes.string ]),
};

InfoModal.defaultProps = {

}
