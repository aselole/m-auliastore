import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { ActionSheetIOS, Platform, StyleSheet, View, Animated, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import AppConfig from 'Config/AppConfig'
import ImagePickers from 'react-native-image-crop-picker'
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons'
import BottomSheet from 'react-native-js-bottom-sheet'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ImageLoading from './ImageLoading'
import ImageFitLoading from './ImageFitLoading'
import ImageGrid from './ImageGrid'
import ButtonIcon from './ButtonIcon'
var _ = require('lodash')

export default class ImagePicker extends Component {

  constructor(props) {
    super(props)
    this.state = {
      image: this.props.data ? this.props.data : [],
    }
  }

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    return true
  }

  // tampilkan opsi cara ambil gambar untuk ios
  _onOpenOptionIOS = () => {
    const options = [
      'Kamera',
      'Pilih dari Galeri Foto',
      'Batal'
    ]
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex: 2
      },
      (buttonIndex: number) => {
        if (buttonIndex === 0) {
          ImagePickers.openCamera({}).then((image: Object) =>
            this._saveImages(image)
          )
        } else if (buttonIndex === 1) {
          ImagePickers.openPicker({
            multiple: this.props.isMultiplePick ? true : false
          }).then(image =>
            this._saveImages(image)
          )
        }
      }
    )
  }

  // tampilkan opsi cara ambil gambar untuk android
  _onOpenOptionAndroid = (): Array<Object> => {
    const options = ['Kamera', 'Pilih dari Galeri Foto']
    return [
      {
        title: options[0],
        onPress: () =>
          ImagePickers.openCamera({}).then((image: Object) =>
            this._saveImages(image)
          ) && this.bottomSheet.close(),
        icon: (
          <MaterialIcons name={'photo-camera'} size={24} style={styles.icon} />
        )
      },
      {
        title: options[1],
        onPress: () =>
          ImagePickers.openPicker({
            multiple: this.props.isMultiplePick ? true : false
          }).then(image =>
            this._saveImages(image)
          ) && this.bottomSheet.close(),
        icon: (
          <MaterialIcons name={'photo-library'} size={24} style={styles.icon} />
        )
      }
    ]
  }

  // crop satu image yang baru dipilih
  _onOpenCropImage() {
    ImagePickers.openCropper({
      path: this.state.image[0],
      width: 200,
      height: 200,
      cropperCircleOverlay: true,
      cropperChooseText: 'Ok',
      cropperCancelText: 'Batal'
    }).then(image => {
      this._replaceImages(image)
    });
  }

  // ganti dengan image yang hasil dicrop
  _replaceImages = (image) => {
    let outputImages = []
    outputImages.push(this._getImagePath(image))
    this.setState({ image: outputImages }, () => {
      this.props.onChange(this.state.image)
    })
  }

  // simpan image ke state
  _saveImages = (images) => {
    let newImages = []
    if(_.isArray(images)) images.map((item) => newImages.push(this._getImagePath(item)))
    else newImages.push(this._getImagePath(images))
    let outputImages = this.props.isMultiplePick ?
                          _.concat(this.state.image, newImages) :
                          newImages
    this.setState({ image: outputImages }, () => {
      if(this.props.isCropping) this._onOpenCropImage()
      if(!this.props.isCropping) this.props.onChange(this.state.image)
    })
  }

  // tambahkan ekstensi file didepan path image nya
  _getImagePath(image) {
    if(_.isNull(image)) return ''
    return 'file://'+image.path
  }

  // tereksekusi ketika image ada yg terhapus, sebab fitur hapus pada ImageGrid
  _onDataChange = (image) => {
    this.setState({ image: image })
    this.props.onChange(image)
  }

  // mulai mengambil image
  _startPick() {
    if(Platform.OS === 'ios') this._onOpenOptionIOS()
    else this.bottomSheet.open()
  }

  // render image gallery untuk multiple image
  _renderGallery = () => {
    if(!this.props.isCustomComponent) {
      if(this.props.isMultiplePick) {
        return (
          <View>
            { this.props.isShowGallery &&
              <ImageGrid
                data={this.state.image}
                onDataChange={this._onDataChange}
                styleContainer={{flex:1, height: 500}}
              />
            }
            <View style={this.props.styleButtonContainer}>
              { this.props.isShowCancelButton &&
                <ButtonIcon
                  title={this.props.titleButtonCancel}
                  onPress={this.props.onPressCancel}
                  icon={Images.camera}
                  bgColor={this.props.colorButtonCancel}
                  styleContainer={[{flex:1}, this.props.styleButtonCancel]}
                />
              }
              <ButtonIcon
                title={this.props.titleButtonOk}
                onPress={Platform.OS === 'ios' ? this._onOpenOptionIOS : () => this.bottomSheet.open()}
                icon={Images.camera}
                bgColor={this.props.colorButtonOk}
                styleContainer={[{flex:1}, this.props.styleButtonOk]}
              />
            </View>
          </View>
        )
      } else {
        return (
          <ImageFitLoading
            path={this.state.image[0]}
            urlDefault={Images.add_image}
            onPress={Platform.OS === 'ios' ? this._onOpenOptionIOS : () => this.bottomSheet.open()}
          />
        )
      }
    } else {
      return (
        <this.props.customComponent onStartPick={() => this._startPick()} />
      )
    }
  }

  render () {
    return (
      <View style={this.props.styleContainer}>
        { this.props.title &&
          <Text style={[{color: 'gray'}]}>{this.props.title}</Text>
        }
        {
          this._renderGallery()
        }
        {Platform.OS === 'android' ? (
          <BottomSheet
            ref={(ref: any) => { this.bottomSheet = ref }}
            title={this.props.titleBottomSheet}
            options={this._onOpenOptionAndroid()}
            coverScreen={true}
          />
        ) : null}
      </View>
    )
  }
}

const styles = StyleSheet.create({ });

ImagePicker.propTypes = {
  onChange: PropTypes.func,
  title: PropTypes.string,
  titleBottomSheet: PropTypes.string,
  data: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]), // format: {path: xxx.jpg, type: xxx}
  styleContainer: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleButtonContainer: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleButtonCancel: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array ]),
  styleButtonOk: PropTypes.oneOfType([ PropTypes.string, PropTypes.number, PropTypes.object, PropTypes.array ]),
  colorButtonCancel: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  colorButtonOk: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  isCropping: PropTypes.bool,     // kasih fitur crop image, hanya bisa diaktifkan kalau isMultiple diset ke false
  isShowGallery: PropTypes.bool,  // tampilkan galeri foto atau tidak
  isShowCancelButton: PropTypes.bool, // tampilkan tombol batal disebelah tombol ambil image
  onPressCancel: PropTypes.func,
  onPressPicker: PropTypes.func,
  titleButtonOk: PropTypes.string,
  titleButtonCancel: PropTypes.string,
  isMultiplePick: PropTypes.bool,
  isCustomComponent: PropTypes.bool,
  customComponent: PropTypes.object,
}

ImagePicker.defaultProps = {
  isMultiple: true,
  isCropping: false,
  isShowGallery: true,
  isShowCancelButton: false,
  colorButtonCancel: 'gray',
  colorButtonOk: 'blue',
  titleButtonOk: 'Ambil Foto',
  titleButtonCancel: 'Batal',
  titleBottomSheet: '',
  isMultiplePick: true,
  isCustomComponent: false,
}
