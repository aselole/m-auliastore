import React, { Component } from 'react'
import { Platform, StyleSheet, Animated, View, TouchableOpacity, Text, Picker } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'

var UIPICKER_HEIGHT = 216;

export default class DropdownCollapse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapsed: true,
      height: new Animated.Value(0)
    };
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.locals.isCollapseParent !== nextProps.locals.isCollapseParent) {
      Animated.timing(
        this.state.height,
        Object.assign(
          { toValue: this.state.isCollapsed ? UIPICKER_HEIGHT : 0 },
          { duration: 200 }
        )
      ).start();
      this.setState({ isCollapsed: !this.state.isCollapsed });
    }
  }

  toggleCollapsible() {
    Animated.timing(
      this.state.height,
      Object.assign(
        { toValue: this.state.isCollapsed ? UIPICKER_HEIGHT : 0 },
        { duration: 200 }
      )
    ).start();
    this.setState({ isCollapsed: !this.state.isCollapsed });
  }

  render() {
    let pickerContainer = styles.pickerContainerNormal;
    let pickerContainerOpen = styles.pickerContainerOpen;
    let selectStyle = styles.selectNormal;
    let touchableStyle = styles.pickerTouchableNormal;
    let touchableStyleActive = styles.pickerTouchableActive;
    let pickerValue = styles.pickerValueNormal;

    if(!this.props.locals.options || this.props.locals.options.length == 0) {
      return null;
    }

    const options = this.props.locals.options.map((item, key) => (
      <Picker.Item key={item.ID} value={item.ID} label={item.NAMA} />
    ));
    const selectedOption = this.props.locals.selectedValue == 0 ?
      this.props.locals.options[0] :
      this.props.locals.options.find(
        option => option.ID === this.props.locals.selectedValue
      );

    return (
      <View style={[ pickerContainer ]}>
        <TouchableOpacity
          style={[
            touchableStyle,
            this.state.isCollapsed ? {} : touchableStyleActive,
          ]}
          onPress={() => this.toggleCollapsible() }
        >
        { selectedOption &&
          <Text style={this.props.locals.valueStyle} numberOfLines={1}>{selectedOption.NAMA}</Text>
        }
        </TouchableOpacity>
        <Animated.View
          style={{ height: this.state.height, overflow: "hidden" }}
        >
          <Picker
            accessibilityLabel={this.props.locals.label}
            ref="input"
            style={selectStyle}
            selectedValue={this.props.locals.selectedValue}
            onValueChange={this.props.locals.onSelected}
            help={this.props.locals.help}
            enabled={this.props.locals.enabled}
            mode={this.props.locals.mode}
            prompt={this.props.locals.prompt}
            itemStyle={this.props.locals.itemStyle}
          >
            {options}
          </Picker>
        </Animated.View>
      </View>
    );
  }
}

var LABEL_COLOR = "#000000";
var INPUT_COLOR = "#000000";
var ERROR_COLOR = "#a94442";
var HELP_COLOR = "#999999";
var BORDER_COLOR = "#cccccc";
var DISABLED_COLOR = "#777777";
var DISABLED_BACKGROUND_COLOR = "#eeeeee";
var FONT_SIZE = 17;
var FONT_WEIGHT = "500";

const styles = StyleSheet.create({
  touchable: {
    borderColor: Colors.primary,
    borderWidth: 1,
    backgroundColor: Colors.snow,
    borderRadius: 5,
    height: Metrics.doubleBaseSize,
    flex:1,
    height: 120,
  },
  title: {
    margin: 10,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.medium
  },
  subtitle: {
    margin: Metrics.smallMargin,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.bold
  },
  images: {
    width: Metrics.smallImage,
    height: Metrics.smallImage,
    alignSelf: 'center',
    marginTop: Metrics.baseMargin
  },
  buttons: {
    margin: Metrics.baseMargin,
    backgroundColor: Colors.primary,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonTitle: {
    margin: 10,
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.medium
  },

  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 19,
    paddingVertical: 14
  },
  highlightBox: {
    borderRadius: 12,
  },
  leftCol: {
    flexDirection: 'row',
    flex: 1.5,
    alignSelf: 'flex-start'
  },
  centerInfo: {
    flex: 5,
  },
  rightCol: {
    flex: 0.5,
    alignSelf: 'flex-start'
  },
  icon: {
    width: 41,
    alignSelf: 'flex-start'
  },
  pickerContainerNormal: {
    marginBottom: 1,
    borderRadius: 4,
    borderColor: BORDER_COLOR,
    borderWidth: 0
  },
  pickerContainerError: {
    marginBottom: 4,
    borderRadius: 4,
    borderColor: ERROR_COLOR,
    borderWidth: 0
  },
  pickerContainerOpen: {

  },
  selectNormal: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR
      },
      ios: {}
    })
  },
  selectError: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR
      },
      ios: {}
    })
  },
  pickerTouchableNormal: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableError: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableActive: {
    borderBottomWidth: 1,
    borderColor: BORDER_COLOR
  },
  pickerValueNormal: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  pickerValueError: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  formGroupNormal: {
    marginBottom: 0
  },
  formGroupError: {
    marginBottom: 10
  },
  controlLabelNormal: {
    color: LABEL_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  controlLabelError: {
    color: ERROR_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  helpBlockNormal: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  helpBlockError: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: ERROR_COLOR
  }
});

DropdownCollapse.propTypes = {
  locals: PropTypes.object
};
