import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import ImageLoading from './ImageLoading'
import ImageFitLoading from './ImageFitLoading'
import Swiper from 'react-native-swiper'
import Parallax from 'Lib/react-native-parallax'
var _ = require('lodash')

export default class ImageSwipe extends Component {

  constructor(props) {
    super(props)
    this.state = {}
  }

  _getImage(input) {
    let image = null
    if(_.isObject(input)) image = input[this.props.itemKey]
    else image = input
    return image
  }

  _renderItem = (item, index) => {
    let image = this._getImage(item)
    if(this.props.isParallax) {
      return (
        <Parallax.ScrollView
          key={index}
          style={{flex:1}}>
          <Parallax.Image
           onPress={() => this._onParallaxClicked(item)}
           source={{ uri: image }}
           resizeMode='contain'
           parallaxFactor={0.8}
           style={styles.parallaxContainer}
           imageStyle={styles.parallaxImage}
           overlayStyle={styles.parallaxOverlay}>
         </Parallax.Image>
        </Parallax.ScrollView>
      )
    } else {
      return (
        <View
         key={index}
         style={styles.sliderContent}>
         <ImageLoading
           style={[styles.imageContent]}
           name={image}
           path={image}
           urlDefault={this.props.defaultImage}
           urlBase={this.props.remoteBaseUrl} />
        </View>
      )
    }
  }

  render () {
    return (
      <Swiper
        dot={<View style={styles.dot}/>}
        activeDot={<View style={styles.dotActive}/>}
        paginationStyle={{backgroundColor:'rgba(0,0,0,0)', top: 0, left: 0}}>
        { !_.isNil(this.props.data) &&
          this.props.data.map((item, index) => this._renderItem(item, index)) }
      </Swiper>
    )
  }
}

const styles = StyleSheet.create({
  dot: {
    backgroundColor: 'rgba(255,255,255,.3)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4
  },
  dotActive: {
    backgroundColor: '#fff',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 4,
    marginRight: 4
  },
  imageContent: {
    flex:1
  },
  sliderContent: {
    width: Metrics.deviceWidth,
    height: Metrics.deviceWidth / 2,
  },
  parallaxContainer: {
    width: Metrics.deviceWidth,
    height: Metrics.deviceHeight * (2/5),
  },
  parallaxImage: {
    width: Metrics.deviceWidth,
    height: Metrics.deviceHeight * (2/5),
  },
  parallaxOverlay: {
    flexDirection: 'column',
    backgroundColor: 'rgba(0,0,0,0.2)'
  },
});

ImageSwipe.propTypes = {
  data: PropTypes.array,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  defaultImage: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  remoteBaseUrl: PropTypes.string,
  itemKey: PropTypes.string,
  isParallax: PropTypes.bool,
}

ImageSwipe.defaultProps = {
  isParallax: false,
}
