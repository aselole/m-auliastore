import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, StyleSheet, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import ImageFitLoading from './ImageFitLoading'

export default class ToolbarButton extends Component {

  constructor(props) {
    super(props)
  }

  render () {
    return (
      <View style={[styles.container, this.props.styleContainer]}>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={[styles.contentContainer, this.props.styleContentContainer]}>
            <ImageFitLoading
              urlDefault={this.props.image}
              style={[styles.icon, this.props.styleIcon]} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.5,
  },
  contentContainer: {
    flex: 1
  },
  icon: {
    flexGrow: 1,
    alignSelf: 'center',
  }
});

ToolbarButton.propTypes = {
  onPress: PropTypes.func,
  image: PropTypes.number,
  styleIcon: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
  styleContentContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.number ]),
}

ToolbarButton.defaultProps = {

}
