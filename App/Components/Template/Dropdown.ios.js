import React, { Component } from 'react'
import { Platform, StyleSheet, Image, Animated, View, TouchableOpacity, Text, Picker } from 'react-native'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome'
import DropdownCollapse from './DropdownCollapse'

export default class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCollapseParent: true,
    };
  }

  getConfig() {
    return {
      title: this.props.title,
      options: this.props.data,
      selectedValue: this.props.selectedValue,
      label: this.props.prompt,
      onSelected: this.props.onSelected,
      help: this.props.help,
      enabled: this.props.enabled,
      mode: this.props.mode,
      prompt: this.props.prompt,
      isCollapseParent: this.state.isCollapseParent,
      itemStyle: this.props.itemStyle,
      valueStyle: this.props.valueStyle,
      titleStyle: this.props.titleStyle
    };
  }

  render() {
    let formGroupStyle = styles.formGroupNormal;
    let controlLabelStyle = styles.controlLabelNormal;
    let selectStyle = styles.selectNormal;
    let helpBlockStyle = styles.helpBlockNormal;
    let errorBlockStyle = styles.errorBlock;

    const label =
      this.props.prompt ? ( <Text style={this.props.titleStyle}>{this.props.prompt}</Text> ) : null;
    const help =
      this.props.help ? ( <Text style={helpBlockStyle}>{this.props.help}</Text> ) : null;

    if(!this.props.data || this.props.data.length == 0) {
      return null;
    }

    var options = this.props.data.map((item, key) => (
      <Picker.Item key={item.ID} value={item.ID} label={item.NAMA} />
    ));

    return (
      <View style={[ this.props.styleContainer ]} >
        <View style={styles.detailItemBox}>

          <View style={styles.leftCol}>
            <View style={styles.icon}>
              <Image source={this.props.imgLeft} style={{ marginTop: 6, width:30, height:30}} />
            </View>
            <View style={{ marginTop: 9, width: 0.7, height: 25, backgroundColor: 'rgb(229,229,229)'}} />
          </View>

          <TouchableOpacity style={styles.centerInfo}
            onPress={() => { this.setState({ isCollapseParent: !this.state.isCollapseParent }) }}>
            <View style={formGroupStyle}>
              {label}
                <DropdownCollapse locals={this.getConfig()} />
              {help}
            </View>
          </TouchableOpacity>

          <TouchableOpacity style={[ styles.rightCol, {marginTop: 13} ]}
            onPress={() => { this.setState({ isCollapseParent: !this.state.isCollapseParent }) }}>
            <Image
              source={this.props.imgRight}
              style={{height: 20, width: null}} />
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}

var LABEL_COLOR = "#000000";
var INPUT_COLOR = "#000000";
var ERROR_COLOR = "#a94442";
var HELP_COLOR = "#999999";
var BORDER_COLOR = "#cccccc";
var DISABLED_COLOR = "#777777";
var DISABLED_BACKGROUND_COLOR = "#eeeeee";
var FONT_SIZE = 17;
var FONT_WEIGHT = "500";

const styles = StyleSheet.create({
  touchable: {
    borderColor: Colors.primary,
    borderWidth: 1,
    backgroundColor: Colors.snow,
    borderRadius: 5,
    height: Metrics.doubleBaseSize,
    flex:1,
    height: 120,
  },
  title: {
    margin: 10,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.medium
  },
  subtitle: {
    margin: Metrics.smallMargin,
    textAlign: 'center',
    color: Colors.primary,
    fontSize: Fonts.size.small,
    fontFamily: Fonts.type.bold
  },
  images: {
    width: Metrics.smallImage,
    height: Metrics.smallImage,
    alignSelf: 'center',
    marginTop: Metrics.baseMargin
  },
  buttons: {
    margin: Metrics.baseMargin,
    backgroundColor: Colors.primary,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 5,
  },
  buttonTitle: {
    margin: 10,
    textAlign: 'center',
    color: Colors.snow,
    fontSize: Fonts.size.medium
  },

  detailItemBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 19,
    paddingVertical: 14
  },
  highlightBox: {
    borderRadius: 12,
  },
  leftCol: {
    flexDirection: 'row',
    flex: 1.5,
    alignSelf: 'flex-start'
  },
  centerInfo: {
    flex: 5,
  },
  rightCol: {
    flex: 0.5,
    alignSelf: 'flex-start'
  },
  icon: {
    width: 41,
    alignSelf: 'flex-start'
  },
  pickerContainerNormal: {
    marginBottom: 1,
    borderRadius: 4,
    borderColor: BORDER_COLOR,
    borderWidth: 0
  },
  pickerContainerError: {
    marginBottom: 4,
    borderRadius: 4,
    borderColor: ERROR_COLOR,
    borderWidth: 0
  },
  pickerContainerOpen: {

  },
  selectNormal: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: INPUT_COLOR
      },
      ios: {}
    })
  },
  selectError: {
    ...Platform.select({
      android: {
        paddingLeft: 7,
        color: ERROR_COLOR
      },
      ios: {}
    })
  },
  pickerTouchableNormal: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableError: {
    flexDirection: "row",
    alignItems: "center"
  },
  pickerTouchableActive: {
    borderBottomWidth: 1,
    borderColor: BORDER_COLOR
  },
  pickerValueNormal: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  pickerValueError: {
    fontSize: FONT_SIZE,
    paddingLeft: 7
  },
  formGroupNormal: {
    marginBottom: 0
  },
  formGroupError: {
    marginBottom: 10
  },
  controlLabelNormal: {
    color: LABEL_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  controlLabelError: {
    color: ERROR_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 7,
    fontWeight: FONT_WEIGHT
  },
  helpBlockNormal: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  helpBlockError: {
    color: HELP_COLOR,
    fontSize: FONT_SIZE,
    marginBottom: 2
  },
  errorBlock: {
    fontSize: FONT_SIZE,
    marginBottom: 2,
    color: ERROR_COLOR
  }
});

Dropdown.propTypes = {
  title: PropTypes.string,
  data: PropTypes.array,
  selectedValue: PropTypes.oneOfType([PropTypes.array, PropTypes.number, PropTypes.object, PropTypes.string]),
  onSelected: PropTypes.func,
  help: PropTypes.string,
  enabled: PropTypes.bool,
  mode: PropTypes.string,
  prompt: PropTypes.string,
  itemStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  valueStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  titleStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
  styleContainer: PropTypes.oneOfType([PropTypes.object, PropTypes.array, PropTypes.string]),
};

Dropdown.defaultProps = {
  itemStyle: [ ],
  valueStyle: [ {lineHeight:0, alignSelf: 'flex-start', backgroundColor: 'transparent'} ],
  titleStyle: [ {lineHeight:0} ],
}
