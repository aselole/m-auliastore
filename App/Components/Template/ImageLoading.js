import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import FastImage from 'react-native-fast-image'
import { createImageProgress } from 'react-native-image-progress'
import { getFirstParam, isURLContainImage } from 'Lib/UrlUtils'
import { isLocalImage, isAssetImage } from 'Lib/CheckUtils'
import { isPropsChanged } from 'Lib/CheckUtils'
var _ = require('lodash')

const FastLoadingImage = createImageProgress(FastImage)

export default class ImageLoading extends Component {

  constructor(props) {
    super(props)
    this.state = {
      source: null,
      fetching: false,
      finished: false,
      loaded: 0, total: 0
    }
  }

  componentWillMount() {
    this._showMainImage(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.path, nextProps.path)) {
      this._showMainImage(nextProps)
    }
    if(isPropsChanged(this.props.urlDefault, nextProps.urlDefault)) {
      this._showMainImage(nextProps)
    }
  }

  _showMainImage(props) {
    if(_.isNil(props.path) || _.isEmpty(props.path)) {
      this._showDefaultImage(props)
      return
    }
    if(isAssetImage(props.path)) {
      this.setState({ source: props.path })
    } else if(isLocalImage(props.path)) {
      this.setState({ source: { uri: props.path }})
    } else if(isURLContainImage(props.path)) {
      this.setState({ source: { uri: props.path }})
    } else {
      this.setState({ source:
        { uri: props.urlBase + props.urlNormalizer(props.path) }
      })
    }
  }

  _showDefaultImage(props) {
    if(_.isNil(props.urlDefault)) return
    if(isAssetImage(props.urlDefault)) {
      this.setState({ source: props.urlDefault })
    } else if(isURLContainImage(props.urlDefault)) {
      this.setState({ source: { uri: props.urlDefault }})
    } else {
      this.setState({ source: { uri: props.urlDefault }})
    }
  }

  _onStartFetching() {
    this.setState({ fetching: true, finished: false })
  }

  _onFetchingProgress(e) {
    this.setState({ loaded: e.nativeEvent.loaded, total: e.nativeEvent.total })
  }

  _onFinishFetching() {
    this.setState({ fetching: false, finished: true })
  }

  _onErrorFetching() {
    this._showDefaultImage()
  }

  render () {
    return (
      <View style={[ this.props.styleContainer ]}>
        <FastLoadingImage
          style={[ { flex:1, position:'absolute',top:0,bottom:0,left:0,right:0 }, this.props.style ]}
          borderRadius={this.props.borderRadius}
          source={this.state.source}
          resizeMode={FastImage.resizeMode.cover}
          indicator={Progress.Pie}
          onLoadStart={() => this._onStartFetching()}
          onProgress={e => this._onFetchingProgress(e)}
          onLoad={() => {}}
          onError={() => this._onErrorFetching()}
          onLoadEnd={() => this._onFinishFetching()} />
      </View>
    )
  }
}

const styles = StyleSheet.create({ });

ImageLoading.propTypes = {
  name: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  /*
  * jika path mengandung 'http://' dan '.jpg' atau '.png', maka gunakan path sebagai url image, urlDefault dan urlBase tdk dipakai
  * jika path bertipe angka, maka file tsb berasal dari asset
  * jika path mengadung 'file://', maka file tsb berasal dari storage lokal
  */
  path: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.string ]),
  urlBase: PropTypes.oneOfType([ PropTypes.object, PropTypes.string ]),
  urlDefault: PropTypes.oneOfType([ PropTypes.object, PropTypes.string, PropTypes.number ]),
  urlNormalizer: PropTypes.func,
  style: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  styleContainer: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string, PropTypes.number ]),
  token: PropTypes.string,
  borderRadius: PropTypes.number,
}

ImageLoading.defaultProps = {
  name: 'Foto',
  urlNormalizer: getFirstParam,
  style: { flex: 1 },
  styleContainer: { flex: 1 },
  borderRadius: 1,
}
