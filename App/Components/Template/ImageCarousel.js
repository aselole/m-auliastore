import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Text, Image, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import * as Progress from 'react-native-progress'
import FastImage from 'react-native-fast-image'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import ImageLoading from './ImageLoading'
import ImageFitLoading from './ImageFitLoading'
import ImageFullscreen from './ImageFullscreen'
import { stringToArray } from 'Lib/ManipulationUtils'
var _ = require('lodash')

export default class ImageCarousel extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentImageIndex: 0,
      data: null,
      isShowFullImage: false,
    }
  }

  componentDidMount() {
    this._setupData(this.props.data)
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.data, nextProps.data) && !_.isNil(nextProps.data)) {
      this._setupData(nextProps.data)
    }
  }

  _setupData(input) {
    let images = _.isArray(input) ? input : stringToArray(input)  //ubah url image ke format standar untuk ImageLoading yaitu array
    this.setState({ data: images })
  }

  _onHandleIndexChanged(index) {
    this.setState({ currentImageIndex: index })
  }

  _onPress(item, index) {
    if(this.props.hasOwnProperty('onPress')) {  // onPress behavior specified by user
      this.props.onPress(item, index)
    } else {                                    // onPress will opened fullscreen image
      this.setState({ isShowFullImage: true })
    }
  }

  _onCloseFullScreen() {
    this.setState({ isShowFullImage: false })
  }

  _renderItem({item, index}) {
    if(_.isNil(item)) {
      return (
        <View style={styles.sliderContent}>
          <ImageLoading
            style={[styles.imageContent]}
            urlDefault={this.props.defaultImage}
            borderRadius={this.props.borderRadius} />
        </View>
      )
    } else {
      let image = null
      if(_.isObject(item)) image = item[this.props.itemKey]
      else image = item
      return (
        <TouchableOpacity onPress={() => this._onPress(item, index)}>
          <View style={[ styles.sliderContent, this.props.styleImage ]}>
           <ImageLoading
             style={[styles.imageContent]}
             name={image}
             path={image}
             urlDefault={this.props.defaultImage}
             urlBase={this.props.remoteBaseUrl}
             borderRadius={this.props.borderRadius} />
         </View>
       </TouchableOpacity>
      )
    }
  }

  render () {
    if(this.state.data) {
      return (
        <View style={[ styles.mainContainer, this.props.styleContainer ]}>
          <Carousel
             ref={(c) => { this._carousel = c; }}
             data={this.state.data}
             firstItem={0}
             enableSnap={true}
             renderItem={this._renderItem.bind(this)}
             sliderWidth={sliderWidth}
             itemWidth={itemWidth}
             onSnapToItem={(index) => this._onHandleIndexChanged(index) }
             containerCustomStyle={[ styles.sliderContainer, this.props.styleCarousel ]}
             contentContainerCustomStyle={[ styles.sliderContentContainer, this.props.styleCarouselContent ]}
          />
          { this.props.showPagination &&
            <Pagination
              dotsLength={this.state.data.length}
              activeDotIndex={this.state.currentImageIndex}
              inactiveDotOpacity={0.7}
              inactiveDotScale={0.55}
              containerStyle={styles.pagination}
            />
          }
          <ImageFullscreen
            data={this.state.data}
            isShown={this.state.isShowFullImage}
            currentImage={this.state.currentImageIndex}
            onToggle={this._onCloseFullScreen.bind(this)}
            itemKey={this.props.itemKey}
          />
        </View>
      )
    } else {
      return (
        <View></View>
      )
    }
  }
}

const horizontalMargin = 20
const sliderHeight = Metrics.deviceHeight * (3.2/5)
const sliderWidth = Metrics.deviceWidth
const itemWidth = sliderWidth - (horizontalMargin * 3)
const itemHeight = Metrics.deviceHeight * (3/5)

const styles = StyleSheet.create({
  mainContainer: {
    height: sliderHeight,
  },
  sliderContainer: {
    marginBottom: 0,
    height: itemHeight
  },
  sliderContentContainer: {
    marginBottom: 0,
    height: itemHeight
  },
  sliderContent: {
    width: itemWidth,
    height: itemHeight,
    elevation: 4,
    shadowOffset: {
      width: 2,
      height: 3
    },
    shadowRadius: 6,
    shadowOpacity: 0.5,
    borderRadius: 10,
  },
  imageContent: {
    position: 'relative',
    flex: 1,
    borderRadius: 10,
  },
  deleteContainer: {
    position: 'absolute',
    top: 5,
    right: 5,
    overflow:'visible'
  },
  deleteIcon: {
    width:20,
    height:20
  },
  pagination: {
    paddingTop: 10,
    paddingBottom: 5,
  }
})

ImageCarousel.propTypes = {
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.string ]),
  defaultImage: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  remoteBaseUrl: PropTypes.string,
  itemKey: PropTypes.string,
  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleCarousel: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleCarouselContent: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  styleImage: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  borderRadius: PropTypes.number,
  showPagination: PropTypes.bool,
  onPress: PropTypes.func,
}

ImageCarousel.defaultProps = {
  defaultImage: Images.no_photo,
  borderRadius: 0,
  showPagination: false,
}
