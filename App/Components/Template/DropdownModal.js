import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, StyleSheet, View, Text,
  Image, TextInput, TouchableHighlight, TouchableOpacity
} from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import css from 'Theme/Style'
import Modal from 'react-native-modalbox'
import Picker from 'react-native-wheel-picker'
import { isObject } from 'Lib/ManipulationUtils'
var _ = require('lodash')
var PickerItem = Picker.Item

/*
* DropdownModal berisi TextInput dan Dropdown yg muncul didalam modal
*/

export default class DropdownModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: null,
      selectedValue: this.props.selectedValue,
      selectedNama: '',
      showModal: false,
      styleTitle: {}
    }
  }

  componentDidMount() {
    this._setupDataDropdown(this.props.items)
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.items !== nextProps.items) {
      if(nextProps.items !== null) {
        this._setupDataDropdown(nextProps.items)
      }
    }
  }

  // isi dropdown dengan data yang ditentukan
  _setupDataDropdown(items) {
    if(_.isNull(items)) return
    let output = null
    let defaultValue = {
      [this.props.dataKeyTitle]: 0,
      [this.props.dataValueTitle]: this.props.defaultValue
    }
    output = [defaultValue, ...items]
    this.setState({ items: output }, () => {
      // jika ada default value yg ditentukan oleh parent component
      this._setSelectedValue(this.props.selectedValue)
    })
  }

  // memunculkan modal ketika skr sedang sembunyi dan begitu juga sebaliknya
  _onModalToggle() {
    this.setState((prev) => { return {showModal: !prev.showModal} })
  }

  // dipanggil ketika modal muncul
  _onModalOpened = () => {
    if(this.state.selectedValue === -1) {
      this._setSelectedValue(0)
    }
  }

  // dipanggil ketika modal tertutup
  _onModalClosed = () => {
    this.setState({
      showModal: false,
      styleTitle: this._getTitleColor(this.state.selectedValue)
    })
  }

  // simpan state untuk dropdown yg dipilih dan judul dari teks input
  _setSelectedValue(value) {
    let nama = ''
    if(Number(value) > -1) {
      this.state.items.map((item, i) => {
        if(Number(item[this.props.dataKeyTitle]) === Number(value)) {
          nama = typeof item[this.props.dataValueTitle] === 'object' ?
                  item[this.props.dataValueTitle][this.props.dataValueTitle] :
                  item[this.props.dataValueTitle]
          return
        }
      })
    }
    this.setState({
      selectedValue: value,
      selectedNama: nama,
      styleTitle: this._getTitleColor(value),
    })
  }

  // dipanggil ketika modal masih buka dan user masih memilih value
  _onValueChanged(value, index) {
    this.setState({
      selectedValue: Number(value),
      selectedNama: typeof this.state.items[index][this.props.dataValueTitle] === 'object' ?
                      this.state.items[index][this.props.dataValueTitle][this.props.dataValueTitle] :
                      this.state.items[index][this.props.dataValueTitle],
      styleTitle: this._getTitleColor(value),
    })
    this.props.onValueChange(value, this.state.items[index][this.props.dataValueTitle])
  }

  // warna untuk title, warna dibagi dua yaitu ketika pilih default value dan selain itu
  _getTitleColor(number) {
    return Number(number) > 0 ? this.props.titleStyle : this.props.titleStyleDefault
  }

  render () {
    let containerStyle = !this.props.usingExternalInput ? { height: 38 } : null
    return (
      <View style={containerStyle}>
        { !this.props.usingExternalInput &&
          <TouchableOpacity
            onPress={this._onModalToggle.bind(this)}
            style={[ styles.mainBox, this.props.styleButton ]}>
              <View style={{flex:1, flexDirection:'row'}}>
                <View style={[ styles.leftCol, this.props.leftColStyle ]}>
                  <View style={[styles.iconBox, this.props.imgLeftContainerStyle]}>
                    <Image source={this.props.imgLeft} style={this.props.imgLeftStyle} />
                  </View>
                  { this.props.showSeparator &&
                    <View style={{
                      width: 0.7,
                      height: 25,
                      backgroundColor: 'rgb(229,229,229)'}} />
                  }
                </View>
                <TouchableOpacity
                  onPress={this._onModalToggle.bind(this)}
                  style={styles.centerText}>
                  <TextInput
                    ref='searchText'
                    autoFocus
                    editable={false}
                    pointerEvents="none"
                    placeholder={this.props.title}
                    value={this.state.selectedNama}
                    underlineColorAndroid='transparent'
                    style={[ styles.searchInput, this.state.styleTitle ]}
                    autoCapitalize='none'
                    returnKeyType={'search'}
                    autoCorrect={false}
                    placeholderTextColor={Colors.themeLight}
                    selectionColor={Colors.themeLight}
                    textColor={Colors.themeLight} />
                </TouchableOpacity>
                <View style={styles.rightCol}>
                  <Image source={this.props.imgRight} style={this.props.imgRightStyle} />
                </View>
              </View>
          </TouchableOpacity>
        }
        { this.props.usingExternalInput &&
          <TouchableOpacity
            onPress={this._onModalToggle.bind(this)}
            style={[ styles.mainAltBox, {flex:1} ]}>
            <View pointerEvents='none' style={{flex:1, flexDirection:'row'}}>
              <TextInput
                accessibilityLabel={this.props.externalInputTitle}
                autoFocus
                editable={false}
                value={this.state.selectedNama}
                style={[ this.props.externalInputStyle, {flex:1} ]} />
              <View style={styles.rightCol}>
                <Image source={this.props.imgRight} style={this.props.imgRightStyle} />
              </View>
            </View>
          </TouchableOpacity>
        }

        <Modal
          isOpen={this.state.showModal}
          position={'bottom'}
          backdrop={true}
          coverScreen={true}
          entry={'bottom'}
          backdropColor={'grey'}
          backButtonClose={true}
          swipeToClose={true}
          animationDuration={300}
          onOpened={this._onModalOpened}
          onClosed={this._onModalClosed}
          style={[ styles.modal ]}>
          { this.state.items &&
            <Picker
    					itemStyle={{color:"grey", fontSize:26}}
    					selectedValue={this.state.selectedValue}
    					onValueChange={this._onValueChanged.bind(this)}>
    						{this.state.items.map((item, key) => (
    							<PickerItem
                    key={'key'+key}
                    value={Number(item[this.props.dataKeyTitle])}
                    label={
                      // cek jika data tipe 1 atau tipe 2
                      isObject(item[this.props.dataValueTitle]) ?
                        item[this.props.dataValueTitle][this.props.dataValueTitle] :
                        item[this.props.dataValueTitle]
                    }
                  />
    						))}
    				</Picker>
          }
        </Modal>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    height: 200,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: 8
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  mainBox: {
    flex: 1,
    flexDirection: 'row',
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: Platform.OS === 'ios' ? 4 : 0,
    paddingBottom: Platform.OS === 'ios' ? 4 : 0,
    marginBottom: 10,
  },
  mainAltBox: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 4,
    paddingBottom: 8,
  },
  leftCol: {
    flexDirection: 'row',
    alignItems: 'center',
    width: 45,
  },
  iconBox: {
    width: 37,
  },
  centerText: {
    flex: 1,
    justifyContent: 'center'
  },
  rightCol: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchInput: {
    fontSize: 15,
    lineHeight: 5
  }
});

DropdownModal.propTypes = {
  title: PropTypes.string,
  titleStyleDefault: PropTypes.oneOfType([ PropTypes.object, PropTypes.string, PropTypes.number ]),
  titleStyle: PropTypes.oneOfType([ PropTypes.object, PropTypes.string, PropTypes.number ]),
  imgLeft: PropTypes.number,
  imgLeftStyle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  imgLeftContainerStyle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  imgRight: PropTypes.number,
  imgRightStyle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
  styleButton: PropTypes.object,
  styleModal: PropTypes.object,
  onValueChange: PropTypes.func,
  selectedValue: PropTypes.number,        // value yang terpilih
  items: PropTypes.array,                 // struktur data:
                                          // tipe 1 => [{ID: x, NAMA: y},{ID: x, NAMA: y}]
                                          // tipe 2 => [{ID: x, NAMA: {ID_PARENT:x2, NAMA: y}},{ID: x, NAMA: {ID_PARENT:x2, NAMA: y}}]
  usingExternalInput: PropTypes.bool,     // pakai component utk textinput yg didefine parent (parent textinput)
  externalInputTitle: PropTypes.string,   // judul parent textinput
  externalInputStyle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]), // style parent textinput
  dataKeyTitle: PropTypes.string,         // judul key data dropdown
  dataValueTitle: PropTypes.string,       // judul value data dropdown

  showSeparator: PropTypes.bool,          // tampilkan separator vertical antara icon dan text input atau tidak

  defaultValue: PropTypes.string,
  leftColStyle: PropTypes.oneOfType([ PropTypes.array, PropTypes.object ]),
}

DropdownModal.defaultProps = {
  imgLeft: Images.search,
  imgLeftStyle: {width:30, height:30},
  imgRight: Images.expand,
  imgRightStyle: {width:30, height:30},
  usingExternalInput: false,
  dataKeyTitle: 'ID',
  dataValueTitle: 'NAMA',
  defaultValue: 'Semua',
  selectedValue: -1,
  usingCustomInput: false,
  showSeparator: true,
  titleStyle: {color: 'black'},
  titleStyleDefault: {color: 'gray'},
}
