import React, {Component} from "react"
import {Text, View, TextInput, Image} from "react-native"
import { Colors, Images, Servers, Strings, Fonts } from 'Theme'

export default class Texts extends Component {
    render() {
        return (
            <Text
                color='#aaa'
                style={[this.props.style, {fontFamily:Fonts.type.base}]}
                {...this.props} />
        );
    }

}
