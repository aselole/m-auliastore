import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, Button, Text, Image, TextInput, TouchableOpacity, TouchableHighlight } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import ImageFullscreen from './ImageFullscreen'
import ButtonIcon from './ButtonIcon'
import ImageLoading from './ImageLoading'
import GridView from 'react-native-super-grid'
import FlexImage from 'react-native-flex-image'
import { updateArray, deleteMultipleArrayValue } from 'Lib/ImmutableUtils'
import AppConfig from 'Config/AppConfig'
var _ = require('lodash')

export default class ImageGrid extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentImageIndex: 0,
      dataFullScreen: null,
      dataSelected: null,
      selected: null,
      selectedIndex: null,
      selectMode: false,
      isShowFullImage: false,
      width: (Metrics.deviceWidth / 2) - 30
    }
  }

  componentWillMount() {
    this._onImageChanged(this.props.data)
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.data !== nextProps.data) {
      if(nextProps.data !== null) {
        this._onImageChanged(nextProps.data)
      }
    }
  }

  _onImageChanged(data) {
    if(_.isNull(data)) return
    this.setState({
      dataFullScreen: data,
      dataSelected: this._addPropsToData(data),
      selected: this._setupSelectedState(data)
    })
  }

  _setupSelectedState(data) {
    let output = []
    data.map((item, i) => {
      output.push(false)
    })
    return output
  }

  _addPropsToData(data) {
    let output = []
    data.map((item, i) => output.push({ id: i, url: item}))
    return output
  }

  // jika salah satu image ditekan
  _onPressImage = (item) => {
    if(!this.state.selectMode) {
      this.setState({ isShowFullImage: true, currentImageIndex: item.id })     // tampilkan image dlm tampilan full screen jika ditekan
    } else {
      if(this.props.isEditable) this._updateSelectedStatus(item)                                   // select image
    }
  }

  // menutup tampilan full screen
  _onCloseFullScreen() {
    this.setState({ isShowFullImage: false })
  }

  // select image ketika ditekan agak lama
  _onSelectImage = (item) => {
    if(this.props.isEditable) {
      this._updateSelectedStatus(item)
    }
  }

  // ubah status select/unselect pada image
  _updateSelectedStatus(item) {
    this.setState({
      selected: updateArray(this.state.selected, item.id, !this.state.selected[item.id])
    }, () => {
      let selected = this._setSelectedIndex()
      this._changePressMode(selected)
    })
  }

  // cari dan simpan index image yang telah diselect
  _setSelectedIndex() {
    let temp = []
    this.state.selected.filter((item, i) => {
      if(item) {
        temp.push(i)
        return true
      } else return false
    })
    this.setState({ selectedIndex: temp })
    return temp
  }

  /*
  * ubah select mode,
  * jika ada yg diselect, maka tekan image lain untuk menselect
  * jika tdk ada yg diselect, maka tekan image akan menampilkan image scr full screen
  */
  _changePressMode(item) {
    this.setState({ selectMode: item.length > 0 ? true : false })
  }

  _onDeleteImage() {
    let deleted = deleteMultipleArrayValue(this.state.dataFullScreen, this.state.selectedIndex)
    this.props.onDataChange(deleted)
    this.setState({
      dataFullScreen: deleted,
      dataSelected: this._addPropsToData(deleted),
      selected: this._setupSelectedState(deleted),
      selectMode: false,
    })
  }

  _renderItem = (item) => {
    return (
        <TouchableHighlight
          style={{flex: 1, alignItems:'center', justifyContent:'center'}}
          onPress={() => this._onPressImage(item)}
          onLongPress={() => this._onSelectImage(item)}>
          <View>
            { !this.state.selected[item.id] &&
              <ImageLoading
                urlDefault={AppConfig.defaultPhotoURL}
                urlBase={AppConfig.basePhotoURL}
                path={item.url}
                style={{opacity: 1, width: this.state.width, height: 100}} />
            }
            { this.state.selected[item.id] &&
              <View>
                <ImageLoading
                  urlDefault={AppConfig.defaultPhotoURL}
                  urlBase={AppConfig.basePhotoURL}
                  path={item.url}
                  style={{opacity: .5, width: this.state.width, height: 100}} />
                <Image source={Images.done} style={{position:'absolute', top:5, left:5, width:25, height:25}} />
              </View>
            }
          </View>
        </TouchableHighlight>
    )
  }

  render () {
    return (
      <View style={[ styles.mainContainer, this.props.styleContainer ]}>
        { this.state.dataSelected &&
          <GridView
            style={[ {flex:1}, this.props.styleGrid ]}
            spacing={5}
            items={this.state.dataSelected}
            renderItem={this._renderItem.bind(this)}
          />
        }
        { this.state.selectMode &&
          <ButtonIcon
            title='Hapus'
            onPress={() => this._onDeleteImage()}
            icon={Images.delete}
            bgColor='red'
            style={{ marginBottom: 8 }}
          />
        }
        <ImageFullscreen
          data={this.state.dataFullScreen}
          isShown={this.state.isShowFullImage}
          currentImage={this.state.currentImageIndex}
          onToggle={this._onCloseFullScreen.bind(this)}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  imageContent: {
    flex:1
  },
  imageNormal: {height:100, alignItems:'center', justifyContent:'center', alignSelf:'center'},
  imageSelected: {opacity: .2, height:100, alignItems:'center', justifyContent:'center', alignSelf:'center'},
})

ImageGrid.propTypes = {
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.object, PropTypes.string ]),
  isSourceLocal: PropTypes.bool,
  remoteDefaultUrl: PropTypes.string,
  remoteBaseUrl: PropTypes.string,
  navigator: PropTypes.oneOfType([ PropTypes.object, PropTypes.func ]),
  onDataChange: PropTypes.func,        // memberitahu parent jika ada image yg terhapus
  isEditable: PropTypes.bool,
  styleContainer: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  styleGrid: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
}

ImageGrid.defaultProps = {
  isSourceLocal: false,
  isEditable: true,
}
