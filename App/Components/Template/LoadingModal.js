import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Platform, StyleSheet, View, Text,
  Image, TextInput, TouchableHighlight, TouchableOpacity
} from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import Icon from 'react-native-vector-icons/FontAwesome'
import Modal from 'react-native-modal'
import * as Progress from 'react-native-progress'
import { DotIndicator, WaveIndicator } from 'react-native-indicators'
import Loading from './Loading'
var _ = require('lodash')

export default class LoadingModal extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isLoading: false,
      title: ''
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!_.isEqual(this.props.title, nextProps.title) && !_.isEmpty(nextProps.title)) {
      this.setState({ title: nextProps.title })
    }
    if(!_.isEqual(this.props.isLoading, nextProps.isLoading)) {
      let isArray = Array.isArray(nextProps.isLoading)

      let usedIndex = isArray ? nextProps.isLoading.findIndex((item) => item) : 0
      let isLoading = isArray ? nextProps.isLoading.some((item) => item) : nextProps.isLoading
      let title = isArray ? nextProps.title[usedIndex] : nextProps.title

      this.setState({ isLoading: isLoading, title: title })
    }
  }

  render () {
    return (
      <Modal
        isVisible={this.state.isLoading}
        backdropColor='gray'
        backdropOpacity={0.5}
        animationInTiming={1000}
        onModalHide={() => this.props.onModalHide()}>
        <View style={styles.container}>
          <View style={styles.loading}>
            <Loading
              isInfinite={this.props.isInfinite}
              infiniteLoadingType={this.props.infiniteLoadingType}
              progress={this.props.progress}
              color={this.props.loadingColor} />
          </View>
          <Text style={[ styles.title, this.props.titleStyle ]}>{this.state.title}</Text>
        </View>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    width: Metrics.deviceWidth - 100,
  },
  container: {
    flex:0.3,
    backgroundColor:'white',
    borderRadius: 15,
    elevation: 6,
    shadowOffset: {
      width: 2,
      height: 8
    },
    shadowRadius: 6,
    shadowOpacity: 0.5
  },
  loading: {
    flex:3,
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'transparent'
  },
  title: {
    flex:1,
    alignSelf:'center',
    color:'grey'
  }
});

LoadingModal.propTypes = {
  title: PropTypes.oneOfType([ PropTypes.string, PropTypes.array ]),
  isLoading: PropTypes.oneOfType([ PropTypes.bool, PropTypes.array ]),
  progress: PropTypes.number,
  isInfinite: PropTypes.bool,
  infiniteLoadingType: PropTypes.string,
  loadingColor: PropTypes.string,
  titleStyle: PropTypes.oneOfType([ PropTypes.object, PropTypes.array ]),
  onModalHide: PropTypes.func,
}

LoadingModal.defaultProps = {}
