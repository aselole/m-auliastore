import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, FlatList, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import { ImageLoading, ImageFitLoading, Text } from 'Template'
import { stringToArray } from 'Lib/ManipulationUtils'
import { isPropsChanged } from 'Lib/CheckUtils'
import { convertToRupiah } from 'Lib/NumberUtils'
var _ = require('lodash')

export default class ImageList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      currentImageIndex: 0,
      data: null,
    }
  }

  componentDidMount() {
    this._setupData(this.props.data)
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      this._setupData(nextProps.data)
    }
  }

  _setupData(input) {
    let images = _.isArray(input) ? input : stringToArray(input)  //ubah url image ke format standar untuk ImageLoading yaitu array
    this.setState({ data: images })
  }

  _onPress(item, index) {
    this.props.onPress(item, index)
  }

  _renderItem = ({ item, index }) => {
    let image = null
    if(_.isObject(item)) image = item[this.props.itemKey]
    else image = item
    return (
     <View
       key={index}
       style={[ styles.imageContainer, this.props.styleImageContainer ]}>
       <TouchableOpacity onPress={() => this._onPress(item, index)}>
         <ImageFitLoading
           style={[ styles.imageContent, this.props.styleImageContent]}
           name={image}
           path={image}
           urlDefault={this.props.defaultImage}
           urlBase={this.props.remoteBaseUrl} />
         <Text
           style={this.props.styleImageTitle}
           numberOfLines={2}>
           {item.nama_product}
         </Text>
         <Text
           style={this.props.styleImagePrice}
           numberOfLines={1}>
           {convertToRupiah(item.price)}
         </Text>
       </TouchableOpacity>
     </View>
    )
  }

  render () {
    if(this.state.data) {
      return (
        <FlatList
          horizontal
          data={this.state.data}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => index}
          style={this.props.styleContainer}
          contentContainerStyle={this.props.styleContentContainer}
        />
      )
    } else {
      return (
        <View></View>
      )
    }
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
  },
  imageContainer: {

  },
  imageContent: {

  }
})

ImageList.propTypes = {
  data: PropTypes.oneOfType([ PropTypes.array, PropTypes.string ]),
  defaultImage: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  remoteBaseUrl: PropTypes.string,
  itemKey: PropTypes.string,
  isHorizontal: PropTypes.bool,
  onPress: PropTypes.func,

  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleContentContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleImageContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleImageContent: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleImageTitle: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleImagePrice: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
}

ImageList.defaultProps = {
  defaultImage: Images.no_photo,
  isHorizontal: true,
}
