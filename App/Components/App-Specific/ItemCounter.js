import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Platform, StyleSheet, View, TextInput, TouchableOpacity } from 'react-native'
import { Colors, Metrics, Images } from 'Theme'
import { ImageLoading, ImageFitLoading, Text, TextInputIcon } from 'Template'
import { isPropsChanged } from 'Lib/CheckUtils'
var _ = require('lodash')

export default class ItemCounter extends Component {

  constructor(props) {
    super(props)
    this.state = {
      number: '0',
    }
  }

  componentDidMount() {
    this.setState({ number: this.props.initialNumber })
  }

  componentWillReceiveProps(nextProps) {
    if(isPropsChanged(this.props.data, nextProps.data)) {
      this._setupData(nextProps.data)
    }
  }

  _onSubstractValue() {
    if(!this.props.isEnabled) return
    if(this.state.number !== '0') {
      this.setState((prevState) => {
        return { number: String(parseInt(prevState.number) - 1) }
      }, () => {
        this.props.onValueChanged(this.state.number)
      })
    }
  }

  _onAddValue() {
    if(!this.props.isEnabled) return
    this.setState((prevState) => {
      return { number: String(parseInt(prevState.number) + 1) }
    }, () => {
      this.props.onValueChanged(this.state.number)
    })
  }

  _getTextStyle() {
    if(this.props.isEnabled) return styles.numberText
    else return styles.numberTextDisabled
  }

  render () {
    if(this.props.isVertical) {
      return (
        <View style={[ styles.container, this.props.styleContainer ]}>
          <View style={styles.subContainer}>
            <TouchableOpacity
              onPress={() => this._onAddValue()}>
              <Text style={styles.numberItem}>+</Text>
            </TouchableOpacity>
            <TextInput
              onChangeText={(number) => this.setState({number})}
              value={this.state.number}
              style={[styles.numberItem, this._getTextStyle() ]} />
            <TouchableOpacity
              onPress={() => this._onSubstractValue()}>
              <Text style={styles.numberItem}>-</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    } else {
      return (
        <View style={[ styles.containerHor, this.props.styleContainer ]}>
          <View style={styles.subContainerHor}>
            <TouchableOpacity
              onPress={() => this._onSubstractValue()}>
              <View style={styles.numberItemHorContainer}>
                <Text style={styles.numberItemHor}>-</Text>
              </View>
            </TouchableOpacity>
            <TextInput
              onChangeText={(number) => this.setState({number})}
              value={this.state.number}
              style={[styles.numberItemHor, styles.numberTextHor]} />
            <TouchableOpacity
              onPress={() => this._onAddValue()}>
              <View style={styles.numberItemHorContainer}>
                <Text style={styles.numberItemHor}>+</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  "container": {
    "alignItems": "flex-end",
    "justifyContent": "center",
    "paddingBottom": 6,
    "width": 46
  },
  "subContainer": {
    "borderWidth": 0.8,
    "borderColor": "#ddd",
    "borderRadius": 4,
    "overflow": "hidden",
    "width": 38
  },
  "numberItem": {
    "fontSize": 15,
    "color": "#666",
    "fontWeight": "300",
    "margin": 0,
    "width": 34,
    "height": 26,
    "paddingTop": 4,
    "textAlign": "center"
  },
  "numberText": {
    "color": "#333",
    "fontWeight": "500",
    "backgroundColor": "white",
    "paddingTop": 6,
    "paddingBottom": 6
  },
  "numberTextDisabled": {
    "color": "#8f8f8f",
    "fontWeight": "500",
    "backgroundColor": "white",
    "paddingTop": 6,
    "paddingBottom": 6
  },
  "containerHor": {
    "alignItems": "center",
    "justifyContent": "flex-end",
    "width": 110
  },
  subContainerHor: {
    "borderWidth": 0.8,
    "borderColor": "#ddd",
    "borderRadius": 4,
    "overflow": "hidden",
    height: 38,
    flexDirection: 'row',
    alignItems: 'center',
  },
  numberItemHorContainer: {
    flex: 1,
    "width": 34,
    "height": 26,
    justifyContent: 'center',
  },
  "numberItemHor": {
    "fontSize": 15,
    "color": "#666",
    "fontWeight": "300",
    "textAlign": "center",
    backgroundColor: 'white'
  },
  "numberTextHor": {
    flex: 0.7,
    "color": "#333",
    "fontWeight": "500",
    "backgroundColor": "white",
    "paddingHorizontal": 6
  },
})

ItemCounter.propTypes = {
  onValueChanged: PropTypes.func.isRequired,
  isVertical: PropTypes.bool,
  initialNumber: PropTypes.string,
  isEnabled: PropTypes.bool,

  styleContainer: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleButton: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
  styleInput: PropTypes.oneOfType([ PropTypes.object, PropTypes.array, PropTypes.number ]),
}

ItemCounter.defaultProps = {
  isVertical: true,
  number: '0',
  isEnabled: true,
}
