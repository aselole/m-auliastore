import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
var _ = require('lodash')

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  resetTransaction: null,
  onTransactionReceived: ['response'],
  onTransactionError: ['response'],

  postCommentBerita: ['content', 'id_parent', 'id_newsfeed'],
  updateCart: ['data'],
})

export const TransactionTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  command: null,
  response: null,
  error_info: null,
  error_title: null,
  is_fetching: false,
  loading_title: '',
})

/* ------------- Reducers ------------- */

export const resetTransaction = (state) => INITIAL_STATE

export const onTransactionReceived = (state, { response }) =>
  state.merge({ response, is_fetching: false })

export const onTransactionError = (state, { response }) =>
  state.merge({
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
    is_fetching: false
  })

export const postCommentBerita = (state, { content, id_parent, id_newsfeed }) =>
  state.merge({
    command: 'post_comment',
    loading_title: 'Mengupload Komentar Anda',
    is_fetching: true,
    response: null,
    error_info: null,
    error_title: null,
  })

export const updateCart = (state, { data }) =>
  state.merge({
    command: 'update_cart',
    loading_title: 'Menyimpan Perubahan Keranjang Belanja',
    is_fetching: true,
    response: null,
    error_info: null,
    error_title: null,
  })
/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_TRANSACTION]: resetTransaction,
  [Types.ON_TRANSACTION_RECEIVED]: onTransactionReceived,
  [Types.ON_TRANSACTION_ERROR]: onTransactionError,
  [Types.POST_COMMENT_BERITA]: postCommentBerita,
  [Types.UPDATE_CART]: updateCart,
})
