import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import update from 'immutability-helper'
var _ = require('lodash')

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  resetList: null,
  getCarousel: null,
  getBestSellerHome: null,
  getNewArrivalHome: null,
  getMostReviewHome: null,

  getDetailProduk: ['kode'],

  getListBerita: ['start', 'length'],
  getCommentBerita: ['id_newsfeed', 'id_parent'],

  getCart: null,

  onDataReceived: ['data', 'key'],
  onDataError: ['response'],
  onDetailReceived: ['detail'],
  onDetailError: ['response'],
  onListClicked: ['id'],
  onListItemClicked: ['data_selected'],
})

export const ListTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  id: null,
  data: null,
  data_selected: null,
  detail: null,
  error_info: null,
  error_title: null,
  token: null,
  is_fetching: false,
  key: ''
})

/* ------------- Reducers ------------- */

export const resetList = (state) => INITIAL_STATE

export const getCarousel = (state) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

export const getBestSellerHome = (state) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

export const getNewArrivalHome = (state) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

export const getMostReviewHome = (state) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

export const getDetailProduk = (state, { kode }) =>
  state.merge({ is_fetching: true, detail: null, error_info: null, error_title: null })

export const getListBerita = (state, { start, length }) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

export const getCommentBerita = (state, { id_newsfeed, id_parent }) =>
  state.merge({ is_fetching: true, error_info: null, error_title: null })

export const getCart = (state) =>
  state.merge({ is_fetching: true, data: null, error_info: null, error_title: null })

// mendukung banyak list dalam 1 halaman
export const onDataReceived = (state, { data, key }) =>
  state.merge({
    data: update(_.isNull(state.data) ? {} : state.data, {$merge: {[key]: data}}),
    is_fetching: false
  })

export const onDataError = (state, { response }) =>
  state.merge({
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
    is_fetching: false
  })

export const onDetailReceived = (state, { detail }) =>
  state.merge({ detail, is_fetching: false })

export const onDetailError = (state, { response }) =>
  state.merge({
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
    is_fetching: false
  })

export const onListClicked = (state, { id }) =>
  state.merge({ id })

export const onListItemClicked = (state, { data_selected }) =>
  state.merge({ data_selected })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.RESET_LIST]: resetList,
  [Types.GET_CAROUSEL]: getCarousel,
  [Types.GET_BEST_SELLER_HOME]: getBestSellerHome,
  [Types.GET_NEW_ARRIVAL_HOME]: getNewArrivalHome,
  [Types.GET_MOST_REVIEW_HOME]: getMostReviewHome,
  [Types.GET_LIST_BERITA]: getListBerita,
  [Types.GET_COMMENT_BERITA]: getCommentBerita,
  [Types.GET_DETAIL_PRODUK]: getDetailProduk,
  [Types.GET_CART]: getCart,
  [Types.ON_DATA_RECEIVED]: onDataReceived,
  [Types.ON_DATA_ERROR]: onDataError,
  [Types.ON_DETAIL_RECEIVED]: onDetailReceived,
  [Types.ON_DETAIL_ERROR]: onDetailError,
  [Types.ON_LIST_CLICKED]: onListClicked,
  [Types.ON_LIST_ITEM_CLICKED]: onListItemClicked,
})
