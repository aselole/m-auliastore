import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
var _ = require('lodash')

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  signup: ['username', 'password'],
  signupOk: ['id_user'],
  signupFail: ['response'],
  signin: ['username', 'password'],
  signinOk: ['data_user'],
  signinFail: ['response'],
  isSignedIn: null,
  isSignedInOk: null,
  isSignedInFail: null,
  signout: null,
  forgotPass: ['username'],
  forgotPassOk: ['id_user'],
  forgotPassFail: ['response'],
  updatePass: ['id_user', 'password'],
  updatePassOk: null,
  updatePassFail: ['response'],
  reset: null,
})

export const UserTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  is_fetching: false,
  error_info: null,
  error_title: null,
  ok_signin: null,
  ok_signup: null,
  ok_forgot_pass: null,
  ok_update_pass: null,
  username: '',
  password: '',
  data_user: {},
})

/* ------------- Reducers ------------- */

export const signin = (state, { username, password }) =>
  state.merge({
    is_fetching: true,
    ok_signin: null,
    error_info: null,
    error_title: null,
    data_user: null,
    username,
    password
  })

export const signinOk = (state, { data_user }) =>
  state.merge({ is_fetching: false, ok_signin: true, data_user })

export const signinFail = (state, { response }) =>
  state.merge({
    is_fetching: false,
    ok_signin: false,
    data: null,
    username: null,
    password: null,
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
  })

export const isSignedIn = (state) =>
  state.merge({ is_fetching: true, ok_signin: null })

export const isSignedInOk = (state) =>
  state.merge({ is_fetching: false, ok_signin: true })

export const isSignedInFail = (state) =>
  state.merge({ is_fetching: false, ok_signin: false })

export const signout = (state) => INITIAL_STATE


export const signup = (state, { username, password }) =>
  state.merge({ is_fetching: true, ok_signup: null, username, password })

export const signupOk = (state, { id_user }) =>
  state.merge({ is_fetching: false, ok_signup: true, id_user })

export const signupFail = (state, { response }) =>
  state.merge({
    is_fetching: false,
    ok_signup: false,
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
  })


export const forgotPass = (state, { username }) =>
  state.merge({ is_fetching: true, ok_forgot_pass: null, username })

export const forgotPassOk = (state, { id_user }) =>
  state.merge({ is_fetching: false, ok_forgot_pass: true })

export const forgotPassFail = (state, { response }) =>
  state.merge({
    is_fetching: false,
    ok_forgot_pass: false,
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
  })


export const updatePass = (state, { id_user, password }) =>
  state.merge({ is_fetching: true, ok_update_pass: null, id_user, password })

export const updatePassOk = (state, { }) =>
  state.merge({ is_fetching: false, ok_update_pass: true })

export const updatePassFail = (state, { response }) =>
  state.merge({
    is_fetching: false,
    ok_update_pass: false,
    error_title: _.isNil(response.problem) ? 'ERROR' : response.problem,
    error_info: _.isNil(response.data) ? '' : response.data,
  })


export const reset = (state) => INITIAL_STATE


/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SIGNIN]: signin,
  [Types.SIGNIN_OK]: signinOk,
  [Types.SIGNIN_FAIL]: signinFail,
  [Types.IS_SIGNED_IN]: isSignedIn,
  [Types.IS_SIGNED_IN_OK]: isSignedInOk,
  [Types.IS_SIGNED_IN_FAIL]: isSignedInFail,
  [Types.SIGNOUT]: signout,
  [Types.SIGNUP]: signup,
  [Types.SIGNUP_OK]: signupOk,
  [Types.SIGNUP_FAIL]: signupFail,
  [Types.FORGOT_PASS]: forgotPass,
  [Types.FORGOT_PASS_OK]: forgotPassOk,
  [Types.FORGOT_PASS_FAIL]: forgotPassFail,
  [Types.UPDATE_PASS]: updatePass,
  [Types.UPDATE_PASS_OK]: updatePassOk,
  [Types.UPDATE_PASS_FAIL]: updatePassFail,
  [Types.RESET]: reset,
})
