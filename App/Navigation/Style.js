import { StyleSheet } from 'react-native'
import { Colors } from 'Theme'

export default StyleSheet.create({
  headerLogin: {
    backgroundColor: Colors.transparent
  },
  header: {
		height: 23,
    backgroundColor: Colors.facebook
  },
  icon: {
    width: 23,
		height: 23,
  },
  tabs: {
    backgroundColor: Colors.silver
  },
	indicator: {
		width: 0,
    height: 0,
    backgroundColor: 'transparent'
	},
	label: {
		fontSize: 10
	},
})
