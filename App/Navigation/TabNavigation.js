import React, { Component } from 'react'
import { Image } from 'react-native'
import { StackNavigator, TabNavigator } from 'react-navigation'
import { withMappedNavigationProps } from 'react-navigation-props-mapper'
import { createBottomTabNavigator, createStackNavigator, BottomTabBar } from 'react-navigation-tabs'

import { ToolbarButton } from 'Template'
import { Colors, Images, Strings } from 'Theme'

import Signin from 'Containers/User/Signin'
import Home from 'Containers/Home/Home'
import Detail from 'Containers/Product/Detail'
import ArticleFeed from 'Containers/Article/ArticleFeed'
import ArticleDetail from 'Containers/Article/ArticleDetail'
import Cart from 'Containers/Cart/Cart'

import styles from './Style'
import stylesCommon from 'Theme/StyleCommon'

const TabBarComponent = (props) => (<BottomTabBar {...props} />);

const HomeStack = StackNavigator(
  {
    Home: { screen: withMappedNavigationProps(Home) },
    Detail: { screen: withMappedNavigationProps(Detail) }
  }
)

// it actually styles it's parent navigator, i.e. TabNav
HomeStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarLabel: 'Home',
    tabBarIcon: ({focused, tintColor}) => (
      <ToolbarButton
        image={Images.home}
        styleContainer={{flex:0.8}}
        onPress={() => navigation.navigate('Home')}
      />
    ),
  }
}

const ArticleStack = StackNavigator(
  {
    ArticleFeed: { screen: withMappedNavigationProps(ArticleFeed) },
    ArticleDetail: { screen: withMappedNavigationProps(ArticleDetail) }
  }
)

// it actually styles it's parent navigator, i.e. TabNav
ArticleStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarLabel: 'Artikel',
    tabBarIcon: ({focused, tintColor}) => (
      <ToolbarButton
        image={Images.article}
        styleContainer={{flex:0.8}}
        onPress={() => navigation.navigate('ArticleFeed')}
      />
    ),
  }
}

const CartStack = StackNavigator(
  {
    Cart: { screen: withMappedNavigationProps(Cart) }
  }
)

CartStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarLabel: 'Keranjang',
    tabBarIcon: ({focused, tintColor}) => (
      <ToolbarButton
        image={Images.cart}
        styleContainer={{flex:0.8}}
        onPress={() => navigation.navigate('Cart')}
      />
    )
  }
}

const UserStack = StackNavigator(
  {
    Signin: { screen: withMappedNavigationProps(Signin) }
  }
)

UserStack.navigationOptions = ({ navigation }) => {
  return {
    tabBarLabel: 'Akun',
    tabBarIcon: ({focused, tintColor}) => (
      <ToolbarButton
        image={Images.user}
        styleContainer={{flex:0.8}}
        onPress={() => navigation.navigate('Signin')}
      />
    )
  }
}

const TabNav = createBottomTabNavigator(
  {
    Home: { screen: HomeStack },
    Article: { screen: ArticleStack },
    Cart: { screen: CartStack },
    User: { screen: UserStack }
  },
  {
    initialRouteName: 'Home'
  }
)

// it actually styles it's parent navigator, i.e. PrimaryNav
TabNav.navigationOptions = ({ navigation }) => {
  return {
    header: null,     // Hide the header from PrimaryNav stack, thus Home's header could appeared
  }
}

export default TabNav
