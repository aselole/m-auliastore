import React, { Component } from 'react'
import { Image } from 'react-native'
import { StackNavigator, TabNavigator } from 'react-navigation'
import { withMappedNavigationProps } from 'react-navigation-props-mapper'

import TabNav from './TabNavigation'

import Splash from 'Containers/Splash'

import { Colors, Images, Strings } from 'Theme'
import styles from './Style'

const StackNav = StackNavigator(
  {
    Splash: { screen: withMappedNavigationProps(Splash) }
  },
  {
    initialRouteName: 'Splash'
  }
)

const PrimaryNav = StackNavigator(
  {
    TabNav: { screen: TabNav },
    StackNav: { screen: StackNav }
  },
  {
    initialRouteName: 'StackNav',
  }
)

export default PrimaryNav
