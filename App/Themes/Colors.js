const colors = {
  theme: '#fff',
  themeAccent: '#8f8f8f',
  themeAction: '#4F4F4F',
  themeHighlight: '#c29855',
  themeDark: '#130f09',
}

export default colors
