import {Dimensions, Platform} from 'react-native'

const { width, height } = Dimensions.get('window')

// Used via Metrics.baseMargin
const metrics = {
  deviceWidth: width,
  deviceHeight: height,
  vw: width / 100,
  vh: height / 100,
  vmin: Math.min(width / 100, height / 100),
  vmax: Math.max(width / 100, height / 100),
  marginHorizontal: 10,
  marginVertical: 10,
  section: 25,
  smallMargin: 5,
  baseMargin: 10,
  doubleBaseMargin: 20,
  doubleSection: 50,
  smallSize: 30,
  baseSize: 50,
  smallImage: 20,
  baseImage: 40,
  doubleBaseImage: 60,
  doubleBaseSize: 100,
  horizontalLineHeight: 1,
  hairlineWidth: 1,
  searchBarHeight: 30,
  pickerMargin: 8,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
  navBarHeight: (Platform.OS === 'ios') ? 64 : 54,
  buttonRadius: 4,
  icons: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 45,
    xl: 50
  },
  images: {
    small: 20,
    medium: 40,
    large: 60,
    logo: 200
  },
}

export default metrics
