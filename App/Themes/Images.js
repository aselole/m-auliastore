// leave off @2x/@3x
const images = {
  down: require('Images/ic_down.png'),
  no_photo: require('Images/ic_no_foto.png'),
  cart: require('Images/ic_cart.png'),
  home: require('Images/ic_home.png'),
  article: require('Images/ic_article.png'),
  comment: require('Images/ic_comment.png'),
  info: require('Images/ic_info.png'),
  fail: require('Images/ic_failed.png'),
  left: require('Images/ic_left_black.png'),
  user: require('Images/ic_user.png'),
  password: require('Images/ic_password.png'),
  cancel: require('Images/ic_cancel.png'),
  minus: require('Images/ic_minus.png'),
  plus: require('Images/ic_plus.png'),
  checked: require('Images/ic_checked.png'),
  unchecked: require('Images/ic_unchecked.png'),
  coupon: require('Images/ic_coupon.png'),
}

export default images
