'use strict';

import {Dimensions} from "react-native";

const Constants = {
  AppName: "ACAP",
  URL: {
    root: 'http://mstore.io',
    logo: 'http://beostore.io/wp-content/uploads/2015/05/logoback.png'
  },
  Post: {
    layout_one: 1,
    layout_two: 2,
    layout_three: 3,

    layout_oneColumn: 5,
    layout_twoColumn: 6,
    layout_simpleListView: 7,
  },
  colors: [
    'rgba(58, 75, 133, 0.6)',
    'rgba(188, 59, 36, 0.6)',
    'rgba(57, 174, 84, 0.6)',
    'rgba(188, 59, 36, 0.6)',
    'rgba(141, 114, 91, 0.6)',
    'rgba(128, 140, 141, 0.6)'
  ],
  Menu: {
    Scale: 0,
    Flat: 1,
    FullSize: 2,
    MenuRightBlack: 3
  },
  Window: {
    get width() {
      return Dimensions.get('window').width;
    },
    get height() {
      return Dimensions.get('window').height;
    }
  }
}

module.exports = Constants;
